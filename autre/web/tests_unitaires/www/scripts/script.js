function recupCode() {
    // code:
    let code = document.querySelector('#code-input');
    // test id
    let id_exercice = document.querySelector('#exercise-id');
    // user id
    let user_id;

    // get a user id:
    fetch("https://perso.isima.fr/~thcliquot/tests_unitaires/php/get_id.php")
        .then(response => {
            console.log("GET ::: ", response);
            user_id = response;
        })
        .catch(error => {
            console.log("getIDError:" + error);
        })

    // post code for testing:
    fetch("https://perso.isima.fr/~thcliquot/tests_unitaires/php/getCode.php", {
        method: "POST", body: JSON.stringify({"user_id": user_id, "id_exercise": id_exercice.value, "code": code.value})
    }).then(function (resp) {
        console.log("REPONSE ::: ", resp)
    }).catch(function (err) {
        console.log("ERROR ::: ", err)
    })

    // get result of tests:
    var urlResult = "https://perso.isima.fr/~thcliquot/tests_unitaires/php/getResult.php?user_id=" + user_id.value;
    fetch(urlResult).then(response => response.text()).then(data => {
        console.log("GET ::: ", data);
        document.querySelector('#result-output').innerText = data;
    }).catch(error => {
        console.log(error);
        document.querySelector('#result-output').innerText = error;
    })
}


function displayTestsDetails() {
    let id_exercice = document.querySelector('#exercise-id');
    var urlInfo = "https://perso.isima.fr/~thcliquot/tests_unitaires/php/getInfo.php?id_exercise=" + id_exercice.value;
    fetch(urlInfo).then(response => response.text()).then(data => {
        console.log(data);
        document.querySelector('#test-details').innerText = data;
    }).catch(error => {
        console.log(error);
        document.querySelector('#test-details').innerText = error;
    })
}
