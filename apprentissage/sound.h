/**
 * @file sound.h
 * @author Théo, Antoine, Oscar
 * 
 */


#ifndef __son_h__
#define __son_h__

#include "UI.h"


/**
 * @brief enum d'appelation des sons
 * 
 */

typedef enum sound_t{
  CLING,
  BATTEUR,
  LOOSE
} sound_t;

/**
 * @brief structure pour les sons
 * 
 */

typedef struct sono_t{
  Mix_Chunk ** sound_list;
  int sound_nbr;
  int on;
  Mix_Music * music;
} sono_t;

void initsound(sono_t * sono);
void freesound(sono_t * sono);
void playsound(sono_t * sono, sound_t sound);
void playmusic(sono_t * sono);


#endif