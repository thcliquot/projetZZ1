/**
 * @file engine.h
 * @author Théo, Antoine, Oscar
 * 
 */

#ifndef ENGINE_H
#define ENGINE_H

#include "physics.h"
#include "sound.h"

/**
 * @brief nombre d'objets physique
 * 
 */

#define N 11

/**
 * @brief facteur de diminution de la puissance des batteurs (sans mouvement) en fonctions des distances
 * 
 */

#define ALPHA 0.6

/**
 * @brief facteur de diminution de la puissance des batteurs (avec mouvement) en fonctions des distances
 * 
 */

#define BETA 3

/**
 * @brief actions possibles
 *
 */
typedef enum ACTION {
  NONE = 0, // aucun batteur activé
  L = 1,    // batteur gauche activé
  R = 2,    // batteur droit activé
  LR = 3    // batteurs gauches et droits activés
} ACTION;

/**
 * @brief structure des batteurs
 * 
 */

typedef struct {
  collider_t coll;
  int length;
  float angle;
} bar_t;

/**
 * @brief structure du menu
 * 
 * Le menu comporte les points, et le temps
 * 
 */

typedef struct {
  int score;
  int timerM;
  float timerS;
} menu_part_t;

/**
 * @brief Structure du pinball
 * 
 * Contient tous les paramétres du pinball, objet physique, batteurs ...
 * 
 */

typedef struct {
  int width, height;
  collider_t collTab[N];
  bar_t bar_l;
  bar_t bar_r;

  int bar_l_press;
  int bar_r_press;

  int ang_r;
  int ang_l;

  ball_t ball;

} pinball_part_t;

void modifyBar(bar_t *bar, float ang);
void modifyBar2(collider_t *coll, bar_t *bar);
bar_t initBar(point_t pointA, int length, float angle);
pinball_part_t *initPinball();
void modifyTimer(menu_part_t *menu, float delta);
menu_part_t *initMenu();
void actionOnBar(pinball_part_t *pinball, ACTION action);
collider_t collisionEnv(pinball_part_t *pinball, menu_part_t *menu,
                        sono_t *sono);
void collisionBar(pinball_part_t *pinball, sono_t * sono);

#endif /* ENGINE_H */
