/**
 * @file SDL.c
 * @author Antoine, Théo, Oscar
 * @brief Fichier pour l'affichage graphique
 * 
 */


#include "SDL.h"

float angleT;

/**
 * @brief Fonction d'initialisation des images du pinball
 * 
 * @param graph 
 * @return pinball_part_sdl_t* 
 */

pinball_part_sdl_t *initPinballSDL(graphique_t *graph) {

  pinball_part_sdl_t *pinball = malloc(sizeof(pinball_part_sdl_t));
  pinball->width = 3 * graph->width / 4;
  pinball->height = graph->height;

  initialiserImage(&pinball->spriteT, "spritePinBall.png", graph->rend);
  pinball->rectSprite.w = pinball->rectSprite.h = 500;
  pinball->rectSprite.y = 0;

  initialiserImage(&pinball->ballT, "ball.png", graph->rend);
  initialiserImage(&pinball->fondT, "fondPinball2.png", graph->rend);
  initialiserImage(&pinball->menuT, "menu.png", graph->rend);
  initialiserImage(&pinball->starT, "star.png", graph->rend);
  initialiserImage(&pinball->bumperHitT, "bumperHit.png", graph->rend);
  initialiserImage(&pinball->planetesT, "planetes.png", graph->rend);

  pinball->rectPlanetes.x = pinball->starRect.y = 0;
  pinball->rectPlanetes.w = 750;
  pinball->rectPlanetes.h = 1000;

  pinball->starRect.x = pinball->starRect.y = 0;
  pinball->starRect.w = 750;
  pinball->starRect.h = 1000;

  return pinball;
}

/**
 * @brief modifie l'affichage du temps
 * 
 * @param graph 
 * @param menu 
 * @param menuSDL 
 */

void modifyTimerSDL(graphique_t *graph, menu_part_t *menu,
                    menu_part_sdl_t *menuSDL) {
  if (menuSDL->timerT) {
    SDL_DestroyTexture(menuSDL->timerT);
  }

  sprintf(menuSDL->affTimer, "%d:%.0f", menu->timerM, menu->timerS);
  menuSDL->surf =
      TTF_RenderText_Blended(menuSDL->font, menuSDL->affTimer, menuSDL->color);
  menuSDL->timerT = SDL_CreateTextureFromSurface(graph->rend, menuSDL->surf);

  SDL_SetRenderDrawColor(graph->rend, 75, 75, 75, 255);
}

/**
 * @brief modifie l'affichage du score
 * 
 * @param graph 
 * @param menu 
 * @param menuSDL 
 */

void modifyScoreSDL(graphique_t *graph, menu_part_t *menu,
                    menu_part_sdl_t *menuSDL) {

  if (menuSDL->scoreT) {
    SDL_DestroyTexture(menuSDL->scoreT);
  }
  sprintf(menuSDL->affScore, "Score : %d", menu->score);
  menuSDL->surf =
      TTF_RenderText_Blended(menuSDL->font, menuSDL->affScore, menuSDL->color);
  menuSDL->scoreT = SDL_CreateTextureFromSurface(graph->rend, menuSDL->surf);
}

/**
 * @brief Iinitialise le menu
 * 
 * @param graph 
 * @param menu 
 * @param pathFont 
 * @param fontSize 
 * @param color 
 * @return menu_part_sdl_t* 
 */

menu_part_sdl_t *initMenuSDL(graphique_t *graph, menu_part_t *menu,
                             char *pathFont, int fontSize, SDL_Color color) {

  menu_part_sdl_t *menuSDL = malloc(sizeof(menu_part_sdl_t));
  menuSDL->width = graph->width / 4;
  menuSDL->height = graph->height;
  menuSDL->posX = 3 * graph->width / 4;

  menuSDL->color = color;

  menuSDL->rectScore.w = 8 * menuSDL->width / 10;
  menuSDL->rectScore.h = 2 * menuSDL->width / 10;
  menuSDL->rectScore.x = menuSDL->posX + menuSDL->width / 10;
  menuSDL->rectScore.y = menuSDL->height / 10 + 55;

  menuSDL->rectTimer.w = 4 * menuSDL->width / 10;
  menuSDL->rectTimer.h = 1 * menuSDL->width / 10;
  menuSDL->rectTimer.x = 5 + menuSDL->posX + menuSDL->width / 10;
  menuSDL->rectTimer.y = menuSDL->height / 10;

  menuSDL->scoreT = NULL;
  menuSDL->timerT = NULL;

  menuSDL->font = TTF_OpenFont(pathFont, fontSize);

  modifyScoreSDL(graph, menu, menuSDL);
  modifyTimerSDL(graph, menu, menuSDL);

  return menuSDL;
}

/**
 * @brief Affiche les objets physique
 * 
 * @param graph 
 * @param c 
 * @param p 
 */

void affBumper(graphique_t *graph, collider_t c, pinball_part_sdl_t *p) {
  p->rectSprite.x = c.func * 500;
  p->rectSprite.h = 500;
  SDL_Rect rect;
  if (c.type == CIRCLE) {
    rect.w = rect.h = c.radius * 2;
    rect.x = c.position.x - c.radius;
    rect.y = c.position.y - c.radius;
  }
  SDL_RenderCopy(graph->rend, p->spriteT, &p->rectSprite, &rect);
}

/**
 * @brief Affiche les batteurs
 * 
 * @param graph 
 * @param b 
 * @param p 
 * @param flip 
 */

void affBar(graphique_t *graph, bar_t b, pinball_part_sdl_t *p,
            SDL_RendererFlip flip) {

  float ang;
  float length = b.length + 40;
  p->rectSprite.x = b.coll.func * 500;
  p->rectSprite.h = 170;
  SDL_Rect rect;
  SDL_Point point;
  if (flip == SDL_FLIP_NONE) {
    point.x = 34;
    rect.x = b.coll.pointA.x - 34;
    ang = -b.angle;
  } else {
    point.x = length - 34;
    rect.x = b.coll.pointA.x - length + 34;
    ang = -b.angle + M_PI;
  }
  point.y = 23.5 - 10;
  rect.w = length;
  rect.h = 50;

  rect.y = b.coll.pointA.y - point.y;
  SDL_RenderDrawLine(graph->rend, b.coll.pointA.x, b.coll.pointA.y,
                     b.coll.pointB.x, b.coll.pointB.y);
  SDL_RenderCopyEx(graph->rend, p->spriteT, &p->rectSprite, &rect,
                   ang * 360 / (2 * M_PI), &point, flip);
}

/**
 * @brief Affiche les murs
 * 
 * @param graph 
 * @param c 
 * @param p 
 */

void affWall(graphique_t *graph, collider_t c, pinball_part_sdl_t *p) {

  p->rectSprite.x = c.func * 500;
  p->rectSprite.h = 40;

  vect_t vectAB = vectFromPoints(c.pointA, c.pointB);
  float angle = atanf(vectAB.y / vectAB.x) * 360 / (2 * M_PI);
  angle = c.pointA.x > c.pointB.x ? angle + 180 : angle;
  SDL_Rect rectWall;
  rectWall.x = c.pointA.x;
  rectWall.y = c.pointA.y;
  rectWall.w = sqrt(normVect2(vectAB)) + 5;
  rectWall.h = 10;

  SDL_Point center = (SDL_Point){0, 5};

  SDL_RenderCopyEx(graph->rend, p->spriteT, &p->rectSprite, &rectWall, angle,
                   &center, SDL_FLIP_NONE);
}

/**
 * @brief Affiche le menu
 * 
 * @param graph 
 * @param pinballSDL 
 */

void drawMenu(graphique_t *graph, pinball_part_sdl_t *pinballSDL) {
  SDL_RenderCopy(graph->rend, pinballSDL->menuT, NULL, NULL);
  SDL_RenderPresent(graph->rend);
  SDL_Delay(16.66);
}

/**
 * @brief Affiche toute la SDL
 * 
 * @param graph 
 * @param pinball 
 * @param menu 
 * @param pinballSDL 
 * @param menuSDL 
 */

void drawSDL(graphique_t *graph, pinball_part_t *pinball, menu_part_t *menu,
             pinball_part_sdl_t *pinballSDL, menu_part_sdl_t *menuSDL) {

  SDL_Rect fondRect;
  SDL_Rect fondf;
  fondRect.y = fondRect.x = 0;
  fondRect.w = 3 * graph->width / 4;
  fondRect.h = graph->height;
  fondf.y = fondf.x = 0;
  fondf.w = graph->width;
  fondf.h = graph->height;


  pinballSDL->starRect.x = (pinballSDL->starRect.x + 1) % 9250;
  pinballSDL->rectPlanetes.x = (pinballSDL->rectPlanetes.x + 3) % 14250;
  SDL_RenderCopy(graph->rend, pinballSDL->fondT, NULL, &fondf);

  SDL_RenderCopy(graph->rend, pinballSDL->starT, &pinballSDL->starRect,
                 &fondRect);
  SDL_RenderCopy(graph->rend, pinballSDL->planetesT, &pinballSDL->rectPlanetes,
                 &fondRect);
                 
  //DL_Rect rectBackgroundMenu =
  //    (SDL_Rect){menuSDL->posX, 0, menuSDL->width, menuSDL->height};

  //SDL_RenderFillRect(graph->rend, &rectBackgroundMenu);
  //SDL_RenderDrawRect(graph->rend, &rectBackgroundMenu);

  for (int i = 0; i < N; i++) {
    if (pinball->collTab[i].type == SEGMENT) {

      affWall(graph, pinball->collTab[i], pinballSDL);
    } else {
      affBumper(graph, pinball->collTab[i], pinballSDL);
    }
  }

  affBar(graph, pinball->bar_l, pinballSDL, SDL_FLIP_NONE);
  affBar(graph, pinball->bar_r, pinballSDL, SDL_FLIP_HORIZONTAL);

  for (float angle = 0; angle < 2 * M_PI; angle += M_PI / 4000) {
    SDL_RenderDrawPoint(graph->rend,
                        pinball->ball.structure.x +
                            pinball->ball.structure.radius * cos(angle),
                        pinball->ball.structure.y +
                            pinball->ball.structure.radius * sin(angle));
  }

  modifyTimerSDL(graph, menu, menuSDL);
  modifyScoreSDL(graph, menu, menuSDL);

  SDL_RenderCopy(graph->rend, menuSDL->scoreT, NULL, &menuSDL->rectScore);
  SDL_RenderCopy(graph->rend, menuSDL->timerT, NULL, &menuSDL->rectTimer);

  SDL_Rect ballRect;
  ballRect.w = ballRect.h = pinball->ball.structure.radius * 2;
  ballRect.x = pinball->ball.structure.x - ballRect.w / 2;
  ballRect.y = pinball->ball.structure.y - ballRect.h / 2;

  SDL_RenderCopyEx(graph->rend, pinballSDL->ballT, NULL, &ballRect, angleT,
                   NULL, SDL_FLIP_NONE);

  angleT += pinball->ball.speed.x > 0 ? sqrt(normVect2(pinball->ball.speed))
                                      : -sqrt(normVect2(pinball->ball.speed));
  SDL_Delay(16.66);
}

void bumperBump(graphique_t *graph, pinball_part_sdl_t *p, collider_t *c) {
  if (c != NULL && c->type == CIRCLE) {

    SDL_Rect rect = {c->position.x - c->radius, c->position.y - c->radius,
                     c->radius * 2, c->radius * 2};

    SDL_RenderCopy(graph->rend, p->bumperHitT, NULL, &rect);
  }
}

void freeAll(pinball_part_sdl_t *pinballSDL, menu_part_sdl_t *menuSDL) {

  SDL_DestroyTexture(pinballSDL->spriteT);
  SDL_DestroyTexture(pinballSDL->ballT);
  SDL_DestroyTexture(pinballSDL->fondT);
  SDL_DestroyTexture(pinballSDL->menuT);
  SDL_DestroyTexture(pinballSDL->starT);
  SDL_DestroyTexture(pinballSDL->bumperHitT);
  SDL_DestroyTexture(pinballSDL->planetesT);

  SDL_DestroyTexture(menuSDL->timerT);
  SDL_DestroyTexture(menuSDL->scoreT);
}
