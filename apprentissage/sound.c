/**
 * @file sound.c
 * @author Théo, Antoine, Oscar
 * @brief 
 */

#include "sound.h"

/**
 * @brief Initialise tout les effets sonores et la musique
 * 
 * @param sono 
 */

void initsound(sono_t * sono){
    char *sound_name[] = {"son/cling.wav", "son/batteur.wav", "son/loose.wav"};
    sono->sound_nbr = 3;
    sono->on = 0;

    Mix_Music * music = malloc(sizeof(Mix_Chunk));

    sono->sound_list = malloc(sono->sound_nbr * sizeof(Mix_Chunk *));
    Mix_Chunk * new_sound;
    if( SDL_Init( SDL_INIT_AUDIO ) < 0 ) exit(EXIT_FAILURE);
    if( Mix_OpenAudio( 44100, MIX_DEFAULT_FORMAT, 2, 2048 ) < 0 ) exit(EXIT_FAILURE);

    for(int i=0; i<sono->sound_nbr; i++){
        new_sound = malloc(sizeof(Mix_Chunk));
        new_sound = Mix_LoadWAV(sound_name[i]);
        if( new_sound == NULL ) exit(EXIT_FAILURE);
        sono->sound_list[i] = new_sound;
    }

    music = Mix_LoadMUS("son/pinballmusic.mp3");
    Mix_VolumeMusic(15);
    if( music == NULL ) exit(EXIT_FAILURE);
    sono->music = music;

}

/**
 * @brief libére tout les effets sonores et la musique
 * 
 * @param sono 
 */

void freesound(sono_t * sono){
    for(int i=0;i<sono->sound_nbr;i++){
        Mix_FreeChunk(sono->sound_list[i]);
    }
    free(sono->sound_list);
    Mix_FreeMusic(sono->music);
    Mix_Quit();
}


/**
 * @brief joue les sons demandés
 * 
 * @param sono 
 * @param sound 
 */

void playsound(sono_t * sono, sound_t sound){
    Mix_PlayChannel( -1, sono->sound_list[sound], 0 );
}

/**
 * @brief joue la musique
 * 
 * @param sono 
 */

void playmusic(sono_t * sono){
    if( Mix_PlayingMusic() == 0 )
    {
        Mix_PlayMusic( sono->music, -1);
    }
    else
    {
        if( Mix_PausedMusic() == 1 )
        {
            Mix_ResumeMusic();
        }
        else
        {
            Mix_PauseMusic();
        }
    }
}