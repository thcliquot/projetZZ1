#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "learning.h"
#include "param.h"

int main(int argc, char *argv[]) {

  int graphique = 1;
  int IA = 1;
  SDL_bool running = SDL_TRUE;

  srand(time(NULL));

  int run_number;
  sscanf(argv[1], "%d", &run_number);

  param_t param;

  grid_t grid;
  initGrid(&grid);

  param.state_number =
      grid.angle_number * grid.line_number * grid.column_number + 2;
  param.action_number = 4;

  param.Q1 = createTable(param.state_number, param.action_number);
  param.Q2 = createTable(param.state_number, param.action_number);
  param.Q = createTable(param.state_number, param.action_number);

  run_result_t res;

  pinball_part_t *pinball = initPinball();
  menu_part_t *menu = initMenu();

  pinball_part_sdl_t *pinballSDL = NULL;
  menu_part_sdl_t *menuSDL = NULL;
  graphique_t *graph = NULL;

  sono_t sono;
  initsound(&sono);

  TTF_Init();
  graph = malloc(sizeof(graphique_t));
  initialiserGraphique(graph, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                       1000, 1000);
  pinballSDL = initPinballSDL(graph);
  menuSDL = initMenuSDL(graph, menu, "Voxel.otf", 80,
                        (SDL_Color){180, 180, 180, 255});
  menuSDL->sono = &sono;

  load(argv[2], &param);

  int i = 0;
  while (i < run_number && running) {
    param.eps = calcEps(param.eps, param.iteration_count);
    param.xi = calcXi(param.xi, param.iteration_count);

    if (IA) {
      res = run(&param, pinball, menu, pinballSDL, menuSDL, graph, grid,
                &running, &IA, &graphique);
      learnSarsa(&res, &param, grid);
      if (param.iteration_count % 50000 == 0) {
        save(argv[2], param);
        printf("%d %d \n", i, menu->score);
      }

      i++;

    } else {
      resetEnv(menu, pinball);
      stepPlayer(pinball, menu, pinballSDL, menuSDL, graph, &running, &IA,
                 &graphique);
    }
  }

  save(argv[2], param);
  freeTable(param.Q1, param.state_number);
  freeTable(param.Q2, param.state_number);
  freeTable(param.Q, param.state_number);
  freeAll(pinballSDL, menuSDL);
  freesound(&sono);
  freeGraphique(graph);
}
