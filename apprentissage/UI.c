#include "UI.h"

/**
 *   \file UI.c
 *   \brief code c regroupant certaines fonctions pour manier le côté graphique
 *   de la sdl
 *
 *  Ne concerne pas tous ce qui va toucher au moteur (event ...) et reste assez
 *  globale pour une utilisation dans la plupart des programmes.
 *
 */

void initialiserSDL() {
  if (SDL_Init(SDL_INIT_VIDEO) == -1) {
    fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError());
    exit(-1);
  }
}

void initialiserWindow(SDL_Window **w, int posX, int posY, int x, int y) {
  *w = SDL_CreateWindow("Flood", posX, posY, x, y, SDL_WINDOW_RESIZABLE);

  if (w == 0) {
    fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError());
    /* on peut aussi utiliser SLD_Log() */
  }
}

void initialiserRenderer(SDL_Renderer **r, SDL_Window *w) {
  *r = SDL_CreateRenderer(
      w, -1, SDL_RENDERER_ACCELERATED); /*  SDL_RENDERER_SOFTWARE */

  if (r == 0) {
    fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError());
  }
}

// Initialise une image
void initialiserImage(SDL_Texture **t, char *path, SDL_Renderer *r) {
  int flags = IMG_INIT_JPG | IMG_INIT_PNG;
  int initted = 0;

  initted = IMG_Init(flags);

  if ((initted & flags) != flags) {

    printf("IMG_Init: Impossible d'initialiser le support des formats, JPG et "
           "PNG requis!\n");
    printf("IMG_Init: %s\n", IMG_GetError());
  }

  SDL_Surface *surface = NULL;
  surface = IMG_Load(path);
  if (!surface) {
    printf("IMG_Load: %s\n", IMG_GetError());
  }

  *t = SDL_CreateTextureFromSurface(r, surface);
  SDL_FreeSurface(surface);
}

// initialise un texte
void initialiserTexte(SDL_Texture **t, TTF_Font **font, SDL_Renderer *r,
                      char *path, char *msg) {
  if (TTF_Init() != 0) {
    fprintf(stderr, "Erreur d'initialisation TTF : %s\n", TTF_GetError());
  }
  SDL_Color couleur = {0, 150, 255, 255};
  *font = TTF_OpenFont(path, 80);
  SDL_Surface *surf = TTF_RenderText_Blended(*font, msg, couleur);
  *t = SDL_CreateTextureFromSurface(r, surf);
  SDL_FreeSurface(surf);
}

void initialiserGraphique(graphique_t *g, int posX, int posY, int w, int h) {
  initialiserSDL();
  initialiserWindow(&g->win, posX, posY, w, h);
  initialiserRenderer(&g->rend, g->win);
  SDL_GetWindowSize(g->win, &g->width, &g->height);
  printf("%d et %d\n", g->width, g->height);
}

void freeGraphique(graphique_t *g) {
  SDL_DestroyRenderer(g->rend);
  SDL_DestroyWindow(g->win);
  free(g);
  TTF_Quit();
  SDL_Quit();
}
