/**
 * @file physics.h
 * @author Théo, Oscar, Antoine
 * 
 */

#ifndef PHYSICS_H
#define PHYSICS_H

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

/**
 * @brief énumération pour les textures des objets
 * 
 */

typedef enum {
  NONET = -1,
  BUMP10,
  BUMP20,
  BUMP30,
  BUMP40,
  BUMP50,
  BUMP100,
  FLIPPER,
  WALL
} FUNCTION;

/**
 * @brief structure d'un cercle
 *
 */

typedef struct {
  float radius, x, y;
} circle_t, point_t, vect_t;

/**
 * @brief structure de la ball
 * 
 */

typedef struct {
  vect_t speed;
  vect_t acceleration;
  circle_t structure;
} ball_t;

/**
 * @brief énumération pour le type d'un objet, segment ou cercle
 * 
 */

typedef enum { CIRCLE, SEGMENT } FORM;

/**
 * @brief structure d'un objet physique
 * 
 * Cela peut être un segment ou un cercle
 * 
 */

typedef struct {
  FORM type;
  FUNCTION func;
  float absorption;
  int point;
  union {
    struct {
      int radius;
      vect_t position;
    };
    struct {
      point_t pointA;
      point_t pointB;
    };
  };

} collider_t;

vect_t initVect(float x, float y, int radius);
point_t initPoint(float x, float y, int radius);

collider_t initColliderCircle(float absorption, float radius, vect_t position,
                              int point, FUNCTION f);

collider_t initColliderSegment(float absorption, vect_t pointA, vect_t pointB,
                               int point, FUNCTION f);

ball_t initBall(vect_t speed, vect_t acceleration, circle_t structure);

int isCollisionCollider(ball_t ball, collider_t coll);

vect_t getBallNextPosition(ball_t ball);

void applyPhysics(ball_t *ball);

vect_t calculateRebound(ball_t ball, collider_t coll);

float normVect2(vect_t v);

vect_t vectFromPoints(point_t pointA, point_t pointB);
#endif /* PHYSICS_H */
