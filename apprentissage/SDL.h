#ifndef SDL_H
#define SDL_H

#include "UI.h"
#include "engine.h"
#include "sound.h"

/**
 * @brief structure contenant les informations du pinball pour la sdl
 * 
 */

typedef struct {
  int width, height;
  SDL_Texture *spriteT;
  SDL_Rect rectSprite;
  SDL_Texture *ballT;
  SDL_Texture *fondT;
  SDL_Texture *menuT;
  SDL_Texture *starT;
  SDL_Rect starRect;
  SDL_Texture *bumperHitT;
  SDL_Texture *planetesT;
  SDL_Rect rectPlanetes;

} pinball_part_sdl_t;

/**
 * @brief structure contenant les informations de la sdl pour le menu
 * 
 */

typedef struct {
  int width, height, posX;
  TTF_Font *font;
  SDL_Texture *scoreT;
  SDL_Texture *timerT;

  SDL_Rect rectScore;
  SDL_Rect rectTimer;

  char affScore[120];
  char affTimer[120];
  SDL_Color color;
  SDL_Surface *surf;
  sono_t * sono;

} menu_part_sdl_t;

pinball_part_sdl_t *initPinballSDL(graphique_t *graph);
void modifyTimerSDL(graphique_t *graph, menu_part_t *menu,
                    menu_part_sdl_t *menuSDL);
void modifyScoreSDL(graphique_t *graph, menu_part_t *menu,
                    menu_part_sdl_t *menuSDL);
menu_part_sdl_t *initMenuSDL(graphique_t *graph, menu_part_t *menu,
                             char *pathFont, int fontSize, SDL_Color color);
void affBumper(graphique_t *graph, collider_t c, pinball_part_sdl_t *p);
void affBar(graphique_t *graph, bar_t b, pinball_part_sdl_t *p,
            SDL_RendererFlip flip);
void affWall(graphique_t *graph, collider_t coll, pinball_part_sdl_t *p);
void drawSDL(graphique_t *graph, pinball_part_t *pinball, menu_part_t *menu,
             pinball_part_sdl_t *pinballSDL, menu_part_sdl_t *menuSDL);

void drawMenu(graphique_t *graph, pinball_part_sdl_t *pinballSDL);
void bumperBump(graphique_t *graph, pinball_part_sdl_t *p, collider_t *c);

void freeAll(pinball_part_sdl_t *pinballSDL, menu_part_sdl_t *menuSDL);
#endif /* SDL_H */
