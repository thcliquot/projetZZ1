var searchData=
[
  ['sar_86',['sar',['../unionsar.html',1,'']]],
  ['save_87',['save',['../param_8c.html#a0eed35edd70a1dd74c1c8bc523c758ca',1,'save(const char *filename, param_t param):&#160;param.c'],['../param_8h.html#a0eed35edd70a1dd74c1c8bc523c758ca',1,'save(const char *filename, param_t param):&#160;param.c']]],
  ['savetable_88',['saveTable',['../param_8c.html#a5f8ffb109cdeb434d89675766e4a9188',1,'saveTable(FILE *f, float **Q, int state_number, int action_number):&#160;param.c'],['../param_8h.html#a5f8ffb109cdeb434d89675766e4a9188',1,'saveTable(FILE *f, float **Q, int state_number, int action_number):&#160;param.c']]],
  ['scalarproduct_89',['scalarProduct',['../physics_8c.html#aba1191ba7adf65d79607eb6c9e30f859',1,'physics.c']]],
  ['sdl_2ec_90',['SDL.c',['../SDL_8c.html',1,'']]],
  ['sono_5ft_91',['sono_t',['../structsono__t.html',1,'sono_t'],['../sound_8h.html#a78dbdc8dfb5ac30c528b502c30b28c46',1,'sono_t():&#160;sound.h']]],
  ['sound_2ec_92',['sound.c',['../sound_8c.html',1,'']]],
  ['sound_2eh_93',['sound.h',['../sound_8h.html',1,'']]],
  ['sound_5ft_94',['sound_t',['../sound_8h.html#a202e4e497751296e816d0bd2a76c6623',1,'sound_t():&#160;sound.h'],['../sound_8h.html#a2c50c612b926805267a222a1576ae5d5',1,'sound_t():&#160;sound.h']]],
  ['stack_95',['stack',['../learning_8c.html#ad6004bfd9d8daca838f599b39f2d1d13',1,'stack(linked_list_t *l, sar_t val):&#160;learning.c'],['../learning_8h.html#ad6004bfd9d8daca838f599b39f2d1d13',1,'stack(linked_list_t *l, sar_t val):&#160;learning.c']]],
  ['state_96',['state',['../structstate.html',1,'']]],
  ['state_5fdetect_97',['state_detect',['../env_8c.html#af205d6089cf9912f14b1248975ef6029',1,'state_detect(int posx, int posy, grid_t grid, ball_t ball):&#160;env.c'],['../env_8h.html#af205d6089cf9912f14b1248975ef6029',1,'state_detect(int posx, int posy, grid_t grid, ball_t ball):&#160;env.c']]],
  ['state_5ft_98',['state_t',['../env_8h.html#a6d795d70c71822ed110b77cffa499f13',1,'env.h']]],
  ['statetoline_99',['stateToLine',['../param_8c.html#a8ee03184a3ddef24cbf954e93f5e7d94',1,'stateToLine(state_t state, grid_t grid):&#160;param.c'],['../param_8h.html#a8ee03184a3ddef24cbf954e93f5e7d94',1,'stateToLine(state_t state, grid_t grid):&#160;param.c']]],
  ['step_100',['step',['../env_8c.html#a66ff8b6c9993af8e1632bcf9c617d2ba',1,'step(ACTION action, pinball_part_t *pinball, menu_part_t *menu, state_t prev_state, pinball_part_sdl_t *pinballSDL, menu_part_sdl_t *menuSDL, graphique_t *graph, grid_t grid, SDL_bool *running, int *IA, int *graphique):&#160;env.c'],['../env_8h.html#a66ff8b6c9993af8e1632bcf9c617d2ba',1,'step(ACTION action, pinball_part_t *pinball, menu_part_t *menu, state_t prev_state, pinball_part_sdl_t *pinballSDL, menu_part_sdl_t *menuSDL, graphique_t *graph, grid_t grid, SDL_bool *running, int *IA, int *graphique):&#160;env.c']]],
  ['stepplayer_101',['stepPlayer',['../env_8c.html#a599c1ce140e4cd92692752009a45f50e',1,'stepPlayer(pinball_part_t *pinball, menu_part_t *menu, pinball_part_sdl_t *pinballSDL, menu_part_sdl_t *menuSDL, graphique_t *graph, SDL_bool *running, int *IA, int *graphique):&#160;env.c'],['../env_8h.html#a599c1ce140e4cd92692752009a45f50e',1,'stepPlayer(pinball_part_t *pinball, menu_part_t *menu, pinball_part_sdl_t *pinballSDL, menu_part_sdl_t *menuSDL, graphique_t *graph, SDL_bool *running, int *IA, int *graphique):&#160;env.c']]],
  ['sum_102',['sum',['../param_8c.html#a9183d640e71f178339d3e2944d80344d',1,'sum(param_t *param):&#160;param.c'],['../param_8h.html#a9183d640e71f178339d3e2944d80344d',1,'sum(param_t *param):&#160;param.c']]]
];
