var indexSectionsWithContent =
{
  0: "abcdefgilmnprsuv",
  1: "abcgmprs",
  2: "elpsu",
  3: "acdefgilmnprsuv",
  4: "afrs",
  5: "afs",
  6: "abn"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "typedefs",
  5: "enums",
  6: "defines"
};

var indexSectionLabels =
{
  0: "Tout",
  1: "Structures de données",
  2: "Fichiers",
  3: "Fonctions",
  4: "Définitions de type",
  5: "Énumérations",
  6: "Macros"
};

