var searchData=
[
  ['maxquality_59',['maxQuality',['../learning_8c.html#a4c11edec72882c469150aee5a39d5df3',1,'maxQuality(float *line, int action_number):&#160;learning.c'],['../learning_8h.html#a4c11edec72882c469150aee5a39d5df3',1,'maxQuality(float *line, int action_number):&#160;learning.c']]],
  ['maxqualityindex_60',['maxQualityIndex',['../learning_8c.html#a782a2d834dc4e290de27f8650ee966be',1,'maxQualityIndex(float *line, int action_number):&#160;learning.c'],['../learning_8h.html#a782a2d834dc4e290de27f8650ee966be',1,'maxQualityIndex(float *line, int action_number):&#160;learning.c']]],
  ['menu_5fpart_5fsdl_5ft_61',['menu_part_sdl_t',['../structmenu__part__sdl__t.html',1,'']]],
  ['menu_5fpart_5ft_62',['menu_part_t',['../structmenu__part__t.html',1,'']]],
  ['modifybar_63',['modifyBar',['../engine_8c.html#af2544288e2fb7e3a569794c51bb75577',1,'modifyBar(bar_t *bar, float ang):&#160;engine.c'],['../engine_8h.html#af2544288e2fb7e3a569794c51bb75577',1,'modifyBar(bar_t *bar, float ang):&#160;engine.c']]],
  ['modifyscoresdl_64',['modifyScoreSDL',['../SDL_8c.html#ad662ab8afdd0ec2a698506476988b499',1,'SDL.c']]],
  ['modifytimer_65',['modifyTimer',['../engine_8c.html#a18747d791732dbc2461402e6ae162be6',1,'modifyTimer(menu_part_t *menu, float delta):&#160;engine.c'],['../engine_8h.html#a18747d791732dbc2461402e6ae162be6',1,'modifyTimer(menu_part_t *menu, float delta):&#160;engine.c']]],
  ['modifytimersdl_66',['modifyTimerSDL',['../SDL_8c.html#a2243b2b392857cfef763f30d4e522f14',1,'SDL.c']]]
];
