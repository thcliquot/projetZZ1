/**
 * @file physics.c
 * @author Oscar, Théo, Antoine
 * @brief Fichier des calculs physique
 * 
 */


#include "physics.h"

// float getX(vect_t v) { return v.type == INT ? v.x : v.fx; }
// float getY(vect_t v) { return v.type == INT ? v.y : v.fy; }


vect_t vectSubstraction(vect_t, vect_t)
    __attribute__((weak, alias("vectFromPoints")));

point_t initPoint(float, float, int) __attribute__((weak, alias("initVect")));

/**
 * @brief fonction d'initialisation d'un vecteur
 * 
 * @param x 
 * @param y 
 * @param radius 
 * @return vect_t 
 */

vect_t initVect(float x, float y, int radius) {
  vect_t v;
  v.x = x;
  v.y = y;
  v.radius = radius;
  return v;
}

/**
 * @brief fonction d'initialisation d'un cercle
 * 
 * @param absorption Coeff qui réduit la vitesse
 * @param radius 
 * @param position 
 * @param point donne le nombre de point à gagner en le touchant
 * @param f donne la texture à utiliser
 * @return collider_t 
 */

collider_t initColliderCircle(float absorption, float radius, vect_t position,
                              int point, FUNCTION f) {
  collider_t c;
  c.type = CIRCLE;
  c.absorption = absorption;
  c.radius = radius;
  c.position = position;
  c.point = point;
  c.func = f;
  return c;
}

/**
 * @brief fonction d'initialisation d'un segement
 * 
 * @param absorption Coeff qui réduit la vitesse
 * @param pointA 
 * @param pointB 
 * @param point donne le nombre de point à gagner en le touchant
 * @param f donne la texture à utiliser
 * @return collider_t 
 */

collider_t initColliderSegment(float absorption, vect_t pointA, vect_t pointB,
                               int point, FUNCTION f) {
  collider_t c;
  c.type = SEGMENT;
  c.absorption = absorption;
  c.pointA = pointA;
  c.pointB = pointB;
  c.point = point;
  c.func = f;
  return c;
}

/**
 * @brief initialise la balle
 * 
 * @param speed 
 * @param acceleration 
 * @param structure 
 * @return ball_t 
 */


ball_t initBall(vect_t speed, vect_t acceleration, circle_t structure) {
  ball_t ball;
  ball.speed = speed;
  ball.acceleration = acceleration;
  ball.structure = structure;
  return ball;
}

/**
 * @brief créé un vecteur à partir de deux points
 * 
 * @param pointA 
 * @param pointB 
 * @return vect_t 
 */

vect_t vectFromPoints(point_t pointA, point_t pointB) {
  vect_t v;
  v.x = pointB.x - pointA.x;
  v.y = pointB.y - pointA.y;
  return v;
}

/**
 * @brief produit vectoriel
 * 
 * @param v1 
 * @param v2 
 * @return float 
 */

float normVectorProduct(vect_t v1, vect_t v2) {
  return v1.x * v2.y - v2.x * v1.y;
}

/**
 * @brief produit de deux vecteurs
 * 
 * @param v1 
 * @param v2 
 * @return float 
 */

float scalarProduct(vect_t v1, vect_t v2) { return v1.x * v2.x + v1.y * v2.y; }

/**
 * @brief norm au carré d'un vecteur
 * 
 * @param v 
 * @return float 
 */

float normVect2(vect_t v) { return scalarProduct(v, v); }

/**
 * @brief produit d'un vecteur avec un scalaire
 * 
 * @param v 
 * @param scalar 
 * @return vect_t 
 */

vect_t productWithScalar(vect_t v, float scalar) {
  vect_t u;
  u.x = v.x * scalar;
  u.y = v.y * scalar;
  return u;
}

/**
 * @brief ajout de deux vecteurs
 * 
 * @param v1 
 * @param v2 
 * @return vect_t 
 */

vect_t addVect(vect_t v1, vect_t v2) {
  vect_t v;
  v.x = v1.x + v2.x;
  v.y = v1.y + v2.y;
  v.radius = v1.radius;

  return v;
}

/**
 * @brief Donne la prochaine position de la balle
 * 
 * @param ball 
 * @return vect_t 
 */

vect_t getBallNextPosition(ball_t ball) {
  return addVect(ball.structure, ball.speed);
}

/**
 * @brief applique la physique pour obtenir la prochaine position de la balle
 * 
 * @param ball 
 */

void applyPhysics(ball_t *ball) {
  ball->speed = addVect(ball->speed, ball->acceleration);
  ball->speed.y = getBallNextPosition(*ball).y > 899 - ball->structure.radius &&
                          ball->speed.y < 1
                      ? 0
                      : ball->speed.y;
  ball->structure = addVect(ball->structure, ball->speed);
  if(ball->speed.x > 50){
    ball->speed.x = 50;
  }
  if(ball->speed.x < -50){
    ball->speed.x = -50;
  }
  if(ball->speed.y > 50){
    ball->speed.y = 50;
  }
  if(ball->speed.y < -50){
    ball->speed.y = -50;
  }
}

/**
 * @brief permet de savoir si un point (cercle de rayon 0) est dans un cercle
 * 
 * @param c1 
 * @param c2 
 * @return int 
 */

int isPoint_CircleInCircle(circle_t c1, circle_t c2) {

  vect_t d = vectFromPoints(c1, c2);
  float r = c1.radius + c2.radius;
  if (normVect2(d) < r * r)
    return 1;
  return 0;
}

/**
 * @brief donne la normal à un vecteur
 * 
 * @param ball 
 * @param coll 
 * @return vect_t 
 */

vect_t normal(ball_t ball, collider_t coll) {
  vect_t vectAB = vectFromPoints(coll.pointA, coll.pointB),
         vectAC = vectFromPoints(coll.pointA, ball.structure), normal;

  float v_z, norm;

  v_z = normVectorProduct(vectAB, vectAC);

  normal.x = -vectAB.y * v_z;
  normal.y = vectAB.x * v_z;

  norm = sqrt(normVect2(normal));
  normal = productWithScalar(normal, 1 / norm);

  return normal;
}

/**
 * @brief Calcul s'il y a collision avec un objet
 * 
 * @param ball 
 * @param coll 
 * @return int 1 si collision 0 sinon
 */


int isCollisionCollider(ball_t ball, collider_t coll) {
  if (coll.type == CIRCLE) {
    return isPoint_CircleInCircle(getBallNextPosition(ball), coll.position);
  } else {

    vect_t vectAB = vectFromPoints(coll.pointA, coll.pointB),
           vectAC = vectFromPoints(coll.pointA, getBallNextPosition(ball)),
           vectBC = vectFromPoints(coll.pointB, getBallNextPosition(ball));
    float norm, normSegment;

    norm = normVectorProduct(vectAB, vectAC);
    norm = norm < 0 ? -norm : norm;

    normSegment = sqrt(normVect2(vectAB));

    float dist = norm / normSegment;

    if (dist < getBallNextPosition(ball).radius) {
      float pscal1 = scalarProduct(vectAB, vectAC),
            pscal2 = -scalarProduct(vectAB, vectBC);

      if ((pscal1 >= 0 && pscal2 >= 0) ||
          isPoint_CircleInCircle(coll.pointB, getBallNextPosition(ball)) ||
          isPoint_CircleInCircle(coll.pointA, getBallNextPosition(ball)))
        return 1;

    }
  }

  return 0;
}

/**
 * @brief calcule le rebond sur un segment
 * 
 * @param ball 
 * @param coll 
 * @return vect_t 
 */

vect_t calculateReboundSegment(ball_t ball, collider_t coll) {
  vect_t n = normal(ball, coll),
         u = productWithScalar(n, scalarProduct(ball.speed, n)),
         w = vectSubstraction(u, ball.speed);

  vect_t v = productWithScalar(vectSubstraction(u, w), 1 / coll.absorption);

  if ((coll.pointB.x - coll.pointA.x > 20 ||
       coll.pointB.x - coll.pointA.x < 20) &&
      coll.pointA.x < 450)
    v.x *= coll.absorption;
  return addVect(v, productWithScalar(ball.acceleration, -1.2));
  return v;
}

/**
 * @brief calcule le rebond sur un cercle
 * 
 * @param ball 
 * @param coll 
 * @return vect_t 
 */

vect_t calculateReboundCircle(ball_t ball, collider_t coll) {
  vect_t n = vectFromPoints(ball.structure, coll.position);

  float norm = sqrt(normVect2(n)), loss = 2 / coll.absorption;
  n = productWithScalar(n, 1 / norm);

  return addVect(ball.speed,
                 productWithScalar(n, -scalarProduct(n, ball.speed) * loss));
}

/**
 * @brief calcule un rebond sur un objet
 * 
 * @param ball 
 * @param coll 
 * @return vect_t 
 * 
 * Fait appel à calculateReboundCircle et calculateReboundSegment
 */

vect_t calculateRebound(ball_t ball, collider_t coll) {
  if (coll.type == CIRCLE)
    return calculateReboundCircle(ball, coll);
  else
    return calculateReboundSegment(ball, coll);
}
