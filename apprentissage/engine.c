/**
 * @file engine.c
 * @author Théo, Oscar, Antoine
 * @brief Fichier contenant la création des objets physiques et leur modification
 * 
 */

#include "engine.h"

/**
 * @brief Modifie l'angle des batteurs
 * 
 * @param bar 
 * @param ang 
 */

void modifyBar(bar_t *bar, float ang) {
  bar->angle = ang;
  bar->coll.pointB.x = bar->coll.pointA.x + bar->length * cos(bar->angle);
  bar->coll.pointB.y = bar->coll.pointA.y - bar->length * sin(bar->angle);
}


/**
 * @brief initialise les batteurs
 * 
 * @param pointA 
 * @param length 
 * @param angle 
 * @return bar_t 
 */

bar_t initBar(point_t pointA, int length, float angle) {
  bar_t bar;
  bar.coll = initColliderSegment(1.2, pointA, initVect(0, 0, 0), 0, FLIPPER);
  bar.length = length;
  bar.angle = angle;
  modifyBar(&bar, angle);

  return bar;
}

/**
 * @brief initialise tous les objets du pinball
 * 
 * @return pinball_part_t* 
 */

pinball_part_t *initPinball() {

  pinball_part_t *pinball = malloc(sizeof(pinball_part_t));

  // préparation du terrain (mur,bumper,balle et batteur)

  pinball->collTab[0] =
      initColliderCircle(1, 75, initVect(375, 500, 75), 10, BUMP10);
  pinball->collTab[1] =
      initColliderCircle(1, 35, initVect(100, 350, 35), 20, BUMP10);
  pinball->collTab[2] =
      initColliderSegment(10, initVect(0, 0, 0), initVect(749, 0, 0), 50, WALL);
  pinball->collTab[3] =
      initColliderSegment(1.2, initVect(0, 0, 0), initVect(0, 999, 0), 0, WALL);
  pinball->collTab[4] = initColliderSegment(1.2, initVect(749, 0, 0),
                                            initVect(749, 999, 0), 0, WALL);
  pinball->collTab[5] = initColliderSegment(1.5, initVect(0, 700, 0),
                                            initVect(200, 800, 0), 0, WALL);
  pinball->collTab[6] = initColliderSegment(1.5, initVect(749, 700, 0),
                                            initVect(550, 800, 0), 0, WALL);
  pinball->collTab[7] =
      initColliderCircle(1, 35, initVect(200, 300, 35), 30, BUMP20);
  pinball->collTab[8] =
      initColliderCircle(1, 35, initVect(600, 150, 35), 40, BUMP40);
  pinball->collTab[9] =

      initColliderCircle(1, 35, initVect(450, 200, 35), 50, BUMP50);
  pinball->collTab[10] =

      initColliderCircle(1, 35, initVect(75, 75, 35), 100, BUMP100);

  pinball->bar_l = initBar(initPoint(200, 800, 0), 125, -M_PI / 12);
  pinball->bar_r = initBar(initPoint(550, 800, 0), 125, 13 * M_PI / 12);

  pinball->bar_l_press = 0;
  pinball->bar_r_press = 0;
  pinball->ang_l = pinball->ang_r = 0;

  int alea = rand() % 650 + 50;

  pinball->ball =
      initBall(initVect(0, 0, 0), initVect(0, 0.4, 0), initVect(alea, 50, 25));

  return pinball;
}

/**
 * @brief modifie le temps
 * 
 * @param menu 
 * @param delta 
 */

void modifyTimer(menu_part_t *menu, float delta) {
  menu->timerS += delta * 0.001;
  if (menu->timerS > 60) {
    menu->timerS -= 60;
    menu->timerM++;
  }
}

/**
 * @brief initialise le menu (score et temps)
 * 
 * @return menu_part_t* 
 */

menu_part_t *initMenu() {

  menu_part_t *menu = malloc(sizeof(menu_part_t));
  menu->score = 0;
  menu->timerS = menu->timerM = 0;
  modifyTimer(menu, 0);

  return menu;
}

/**
 * @brief modifie les batteurs en fonction de l'action faite
 * 
 * @param pinball 
 * @param action 
 */

void actionOnBar(pinball_part_t *pinball, ACTION action) {
  if ((action != L) && (action != LR)) {
    pinball->ang_l = pinball->ang_l <= 0 ? 0 : pinball->ang_l - 1;
    pinball->bar_r.coll.absorption = 1.1;
  } else {
    if (pinball->ang_l < 5) {
      pinball->ang_l++;
      pinball->bar_l.coll.absorption = 0.55;
    } else {
      pinball->bar_l.coll.absorption = 1.1;
    }
  }
  if ((action != R) && (action != LR)) {
    pinball->ang_r = pinball->ang_r <= 0 ? 0 : pinball->ang_r - 1;
    pinball->bar_r.coll.absorption = 1.1;
  } else {

    if (pinball->ang_r < 5) {
      pinball->ang_r++;
      pinball->bar_r.coll.absorption = 0.55;
    } else {
      pinball->bar_r.coll.absorption = 1.1;
    }
  }
}

/**
 * @brief calcul les collisions avec l'environnement
 * 
 * @param pinball 
 * @param menu 
 */

collider_t collisionEnv(pinball_part_t *pinball, menu_part_t *menu,
                        sono_t *sono) {
  collider_t retour;

  for (int i = 0; i < N; i++) {
    if (isCollisionCollider(pinball->ball, pinball->collTab[i])) {
      retour = pinball->collTab[i];
      menu->score += pinball->collTab[i].point;

      pinball->ball.speed =
          calculateRebound(pinball->ball, pinball->collTab[i]);
      if (sono->on) {
        if (!isCollisionCollider(pinball->ball, pinball->bar_l.coll)) {
          if (pinball->collTab[i].func != WALL &&
              pinball->collTab[i].func != NONET) {
            playsound(sono, CLING);
          }
        }
      }
    }
  }

  return retour;
}

/**
 * @brief Calcul les collisions avec les batteurs
 * 
 * @param pinball 
 */

void collisionBar(pinball_part_t *pinball, sono_t *sono) {

  if (isCollisionCollider(pinball->ball, pinball->bar_l.coll)) {

    pinball->ball.speed = calculateRebound(pinball->ball, pinball->bar_l.coll);

    if (pinball->ang_l != 0 && pinball->ang_l != 5 && pinball->bar_l_press)
      pinball->ball.speed.y -=
          (pinball->bar_l.coll.pointA.y - pinball->ball.structure.y) * ALPHA +
          (5 - pinball->ang_l) / BETA;

    if (sono->on) {
      if (!isCollisionCollider(pinball->ball, pinball->bar_l.coll)) {
        // playsound(sono, WOOSH);
      }
    }
  }

  if (isCollisionCollider(pinball->ball, pinball->bar_r.coll)) {

    pinball->ball.speed = calculateRebound(pinball->ball, pinball->bar_r.coll);

    if (pinball->ang_r != 0 && pinball->ang_r != 5 && pinball->bar_r_press)
      pinball->ball.speed.y -=
          (pinball->bar_r.coll.pointA.y - pinball->ball.structure.y) * ALPHA +
          (5 - pinball->ang_r) / BETA;

    if (sono->on) {
      if (!isCollisionCollider(pinball->ball, pinball->bar_l.coll)) {
      }
    }
  }
}
