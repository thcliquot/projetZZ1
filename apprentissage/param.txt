sauv 1a:
	2 angles
	reward perdu balle -200
	eps0 = 0.98, raison 0.98, palier 5000
	xi0 = 0.99, raison 0.98, pallier 20000
	gamma = 0.98
	matrice initialisée à 0
sauv 2a:
	2 angles
	reward balle perdue -500
	eps0 = 0.98 raison 0.98 palier 25000
	xi0 = 0.99 raison 0.985 pallier 100000
	gamma = 0.95
	matrice initialisée à 0
sauv 3a:
	8 angles
	reward balle perdue -500
	eps0 = 0.98 raison 0.96 pallier 20000
	xi0 = 0.99 raison 0.98 pallier 80000
	gamma = 0.95
	matrice initialisée à 0
sauv 4a:
	8 angles
	reward balle perdue -1000
	eps0 = 0.98 raison 0.97 pallier 30000
	xi0 = 0.99 raison 0.98 pallier 70000
	gamma = 0.98
	matrice initialisée avec sauv3a
sauv 5a
	double backtracking
	gamma1 = 0.99 gamma2 = 0.25
	eps0 = 0.98 raison 0.96 pallier 20000
	xi0 = 0.99 raison 0.98 pallier 50000
	matrice initialisée à 0;
sauv 6a:
	double backtracking corrigé sans reward négative
	gamma1 = 0.99
	eps0 = 0.98 raison 0.96 pallier 20000
	xi0 = 0.99 raison 0.97 pallier 30000
	matrice initialisée à 0
sauv 7a:
	double backtracking corrigé sans reward négative
	gamma1 = 0.99
	eps0 = 0.98 raison 0.96 pallier 200
	xi0 = 0.99 raison 0.98 pallier 1000
	matrice initialisée à 0
	150 step
sauv 8a:
	double backtracking corrigé sans reward négative
	gamma1 = 0.99
	eps0 = 0.98 raison 0.96 pallier 400
	xi0 = 0.99 raison 0.98 pallier 2000
	matrice initialisée à 0
	150 step
sauv 9a:
	double backtracking corrigé avec reward négative à -1000
	gamma1 = 0.99
	gamma2 = 0.25
	eps0 = 0.98 raison 0.96 pallier 400
	xi0 = 0.99 raison 0.98 pallier 2000
	matrice initialisée à 0
	100 step
sauv10a:
	double backtracking corrigé avec reward negative -2000
	gamma1 = 0.995
	gamma2 = 0.3
	eps0 = 0.98 raison 0.96 pallier 4000
	xi0 = 0.99 raison 0.97 pallier pallier 10000
	matrice initialisée à 0
	150 step
sauv11a:
	idem sauv 10a
	alpha = 0.6
	beta = 3
sauv 12a:
	idem sauv11a
	reward negative -5000
sauv13a:
	idem sauv12a
	reward negative -7000
	gamma2 = 0.35
sauv14a:
	idem sauv13a
sauv15a
	idem sauv13a
	reward negative -10000
sauv16a:
	reward négative 0
	eps0 = 0.98 raison 0.96 pallier 500
	xi0 = 0.99 raison 0.96 pallier 1000
	matrice initialisée à 0
	150 step
	gamma1 = 0.96
sauv17a:
	idem 16a
sauv18a:
	idem 16a
	reward negative -7000
	gamma2 = 0.35
sauv19a:
	grille 32 8 8 2
	reward negative -7000
	gamma1 = 0.98
	gamma2 = 0.35
	eps0 = 0.98 raison 0.98 pallier 4000
	xi0 = 0.99 raison 0.99 pallier 10000
	matrice initialisée à 0
	150 steps
sauv20a:
	idem 13a avec grille 19a
sauv21a:
	idem sauv20a
	gamma1 = 0.995
	gamma2 = 0.4
sauv22a:
	changement physique
	gamma1 = 0.999
	eps0 pallier 8000
	xi0 pallier 15000
