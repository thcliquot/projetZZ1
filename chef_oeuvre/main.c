#include "character.h"
#include "environnement.h"
#include "sdl.h"
#include <SDL2/SDL.h>
#include <stdio.h>

/**
 * @brief Etat du systeme
 * Start = Premier menu de selection de la difficulté
 *
 * Wait_input = Attente de la séléction de l'action choisit
 *
 * Running = boucle sur attente de séléction d'une action et
 * affichage/calcul des actions
 *
 * Over = fin de la partie, si win = 0, on perdu sinon on a gagner
 *
 */

typedef enum STATE { START = 0, WAIT_INPUT = 1, RUNNING = 2, OVER = 3 } STATE;

/**
 * @brief Fonction qui géré la modification de la taille de la fenêtre et
 * l'arrêt prématuré du jeu
 *
 * @param event
 * @param graph
 * @param program_on
 */

void event_global(SDL_Event event, graphique_t *graph, SDL_bool *program_on,
                  texture_t *T) {
  switch (event.type) {
  case SDL_QUIT:
    *program_on = SDL_FALSE;
    break;

  case SDL_KEYDOWN:
    switch (event.key.keysym.sym) {
    case SDLK_ESCAPE:
      *program_on = SDL_FALSE;
      break;
    }
    break;

  case SDL_WINDOWEVENT:
    switch (event.window.event) {
    case SDL_WINDOWEVENT_CLOSE:
      *program_on = SDL_FALSE;
      break;
    case SDL_WINDOWEVENT_SIZE_CHANGED:
      graph->width = event.window.data1;
      graph->height = event.window.data2;
      modifyAll(graph, T);
      break;
    }
    break;
  }
}

/**
 * @brief main
 */

int main() {

  srand(time(NULL));

  SDL_Event event;
  SDL_bool program_on = SDL_TRUE;
  STATE state = 0;
  ACTION actp1, actp2;

  graphique_t *graph = malloc(sizeof(graphique_t));
  texture_t *T = malloc(sizeof(texture_t));

  entity_charac_t p1, p2;

  int current_charac = 0;
  int win = 0;
  int hard = 0;

  init_charac(&p2, charac_ordre[current_charac]);
  init_charac(&p1, 0);

  initialiserGraphique(graph, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                       900, 900);

  initCharacter(graph, 0, T);
  initCharacter(graph, charac_ordre[current_charac], T);
  initTexture(graph, T);

  drawScenery(graph, T);
  drawAcc(graph, T);
  SDL_RenderPresent(graph->rend);

  while (program_on) {
    while (program_on && (state == START)) { // menu démarrer
      while (SDL_PollEvent(&event)) {
        event_global(event, graph, &program_on, T);
        switch (event.type) {
        case SDL_KEYDOWN:
          switch (event.key.keysym.sym) {
          case SDLK_RETURN: // si on appuie sur entrer, on passe en attende
                            // d'input
            state = WAIT_INPUT;
            break;
          case SDLK_UP: // si on appuie sur fléche haut on passe en difficulté
                        // difficile
            hard = 1;
            break;
          case SDLK_DOWN: // si on appuie sur fléche bas on passe en difficulté
                          // facile
            hard = 0;
            break;
          }
          break;
        case SDL_MOUSEBUTTONDOWN:
          if (SDL_BUTTON(SDL_BUTTON_LEFT)) {
            int x, y;
            SDL_GetMouseState(&x, &y);
            if ((graph->height * 5 / 8 <= y && 6 * graph->height / 8 >= y)) {
              if (graph->width * 5 / 8 <= x && graph->width * 7 / 8 >= x) {
                hard = 1;
                state = WAIT_INPUT;
              } else if (graph->width / 8 <= x && (graph->width * 3) / 8 >= x) {
                hard = 0;
                state = WAIT_INPUT;
              }
            }
          }
          break;
        }
      }
      drawScenery(graph, T);
      drawAcc(graph, T);
      SDL_RenderPresent(graph->rend);
      SDL_Delay(40);
    }

    while (
        program_on &&
        ((state == RUNNING) ||
         (state == WAIT_INPUT))) { //état principale, attente input puis calcul

      state = WAIT_INPUT;

      while ((state == WAIT_INPUT) && program_on) { // attente input
        while (SDL_PollEvent(&event)) {
          event_global(event, graph, &program_on, T);

          switch (event.type) {
          case SDL_KEYDOWN:
            switch (event.key.keysym.sym) {
            case SDLK_a:
              actp1 = ATT_PHY;
              state = RUNNING;
              break;
            case SDLK_z:
              if (p1.mp > MP_COST) {
                actp1 = ATT_MAG;
                state = RUNNING;
              }
              break;
            case SDLK_e:
              actp1 = SHIELD;
              state = RUNNING;
              break;
            case SDLK_r:
              if (p1.nbr_pot > 0) {
                actp1 = H_POT;
                state = RUNNING;
              }
              break;
            }
            break;
          case SDL_MOUSEBUTTONDOWN:
            if (SDL_BUTTON(SDL_BUTTON_LEFT)) {
              int x, y;
              SDL_GetMouseState(&x, &y);
              if ((9 * graph->height / 10 <= y &&
                   9 * graph->height / 10 + 90 >= y)) {
                if (graph->width / 8 - 92 <= x && graph->width / 8 + 92 >= x) {
                  actp1 = ATT_PHY;
                  state = RUNNING;
                } else if (3 * graph->width / 8 - 92 <= x &&
                           3 * graph->width / 8 + 92 >= x) {
                  if (p1.mp > MP_COST) {
                    actp1 = ATT_MAG;
                    state = RUNNING;
                  }
                } else if (5 * graph->width / 8 - 92 <= x &&
                           5 * graph->width / 8 + 92 >= x) {
                  actp1 = SHIELD;
                  state = RUNNING;
                } else if (7 * graph->width / 8 - 92 <= x &&
                           7 * graph->width / 8 + 92 >= x) {
                  if (p1.nbr_pot > 0) {
                    actp1 = H_POT;
                    state = RUNNING;
                  }
                }
              }
            }
            break;
          }
        }
        drawScenery(graph, T);
        drawCharacter(graph, T);
        drawBoutonChoix(graph, MP_COST, p1.mp, p1.nbr_pot, T);
        drawLifeBar(graph, p1, p2, current_charac, T);
        drawManaBar(graph, p1, p2, current_charac, T);
        drawHealthPotion(graph, p1.nbr_pot, 0, T);
        drawHealthPotion(graph, p2.nbr_pot, 1, T);
        SDL_RenderPresent(graph->rend);
        SDL_Delay(40);
      }

      if (program_on) {
        markhov(&p1, &p2, &actp2); // calcul de l'action du bot

        playRound(&p1, &p2, actp1, actp2, graph, T); // calcul des param des
                                                     // personnages
      }

      if (p1.hp <= 0) {
        state = OVER;
      } else if (p2.hp <= 0) {
        current_charac += 1;
        if (current_charac < nbr_charac) {
          if (hard == 0) {
            init_charac(&p1, 0);
          }
          init_charac(&p2, charac_ordre[current_charac]);
          initCharacter(graph, charac_ordre[current_charac], T);
        } else {
          win = 1;
          state = OVER;
        }
      }
    }

    SDL_SetRenderDrawColor(graph->rend, 0, 0, 0, 255);
    SDL_RenderClear(graph->rend);

    while (program_on && (state == OVER)) { // etat over, fin du jeu
      while (SDL_PollEvent(&event)) {
        event_global(event, graph, &program_on, T);

        switch (event.type) {
        case SDL_KEYDOWN:
          switch (event.key.keysym.sym) {
          case SDLK_RETURN:
            state = START;
            break;
          }
        }
        if (win) {
          drawWin(graph, T);
        } else {
          drawLoose(graph, T);
        }
        SDL_RenderPresent(graph->rend);
        SDL_Delay(10);
      }
    }
  }

  free(graph);
  freeTexture(T);
  SDL_Quit();

  return 0;
}
