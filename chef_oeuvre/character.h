/**
 * @file character.h
 * @author Oscar MALLET
 * @brief Code h de la manipulation des c
 */

#ifndef character_h
#define character_h

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/**
 * @brief Cout en MP de attack_mag
 *
 */

#define MP_COST 10

/**
 * @brief Heal fait par une potion lors d'un heal
 *
 */

#define POT_HEAL_VAL 20

extern int charac_ordre[4];
extern int nbr_charac;

/**
 * @brief Type/classe du personnage
 */

typedef enum TYPE {
  JOUEUR = 0,
  LEGIONNAIRE = 1,
  BERSERKER = 2,
  PALADIN = 3,
  ARCHER = 4
} TYPE;

/**
 * @brief Type d'action effectuer par le personnage
 */

typedef enum ACTION{
	ATT_PHY = 0,
	ATT_MAG = 1,
	SHIELD = 2,
	H_POT = 3
} ACTION;

/**
 * @brief Ensemble de statistique
 */

typedef struct stats_t{
	int attack;
	int defense;
	int evade;
} stats_t;

/**
 * @brief Ensemble de caractéristique d'un charactére
 * 
 * action = action passé
 */

typedef struct entity_charac_t{
	int place;
	TYPE type;
	int hp;
	int mp;
	int nbr_pot;
	stats_t stat;
	stats_t bonus;
	ACTION action;
} entity_charac_t;

/**
 * @brief Initialise un charactére en fonction du type donné
 * 
 * @param p 
 * @param type 
 */

void init_charac(entity_charac_t * p, TYPE type);

/**
 * @brief Tire l'action suivante du bot selon la table de markhov
 * 
 * @param p1 l'entité 1
 * @param p2 le bot
 * @param act2 valeur retournée
 * 
 * On a besoin de l'action précédente du joueur et de la vie du bot
 */

void markhov(entity_charac_t * p1, entity_charac_t * p2, ACTION * act2);

/**
 * @brief Calcule les nouvelles stats du receiver après une attaque physique
 * 
 * @param sender 
 * @param receiver 
 * @param act action du receiver
 * @return int renvoie 0 pour réussite, 1 pour parade, 2 pour dodge
 * 
 * On retranche les degats totaux dont on a soustrait la defense au hp
 * 
 */

int att_phy(entity_charac_t * sender, entity_charac_t * receiver, ACTION actr);	


/**
 * @brief Calcule les nouvelles stats du receiver après une attaque magique
 * 
 * @param sender 
 * @param receiver 
 * @param actr action du receiver
 * @return int renvoie -1 pour pas de mp, 0 pour réussite, 2 pour dodge
 * 
 * Consomme des mp selon MP_COST
 * L'attaque magique ignore la defense mais ne fait q'une moitié des dégats total
 * On retranche les degats à la vie
 * 
 */

int att_mag(entity_charac_t *sender, entity_charac_t *receiver);

/**
 * @brief Calcule la défense après un action bouclier
 * 
 * @param sender
 * @param act 
 * @return int 0 pour réussite
 * 
 * Le character gagne 100% de sa defense de base en bonus
 */

int def(entity_charac_t * sender);	              
						                                                        

/**
 * @brief Calcule la vie après un action de healing
 * 
 * @param sender
 * @param act
 * @return int -1 pour plus de potion, 0 pour réussite
 * 
 * Heal de HEAL_POT_VAL
 * Consomme une potion
 * 
 */

int heal(entity_charac_t * sender);

/**
 * @brief Mets à jour les bonus
 * 
 * @param p 
 * 
 * Mets à jour la defense bonus
 * Mets à jour le bonus de dégats du berserker
 * 
 */

void update_bonus(entity_charac_t * p);

#endif
