/**
 * @file character.c
 * @author Oscar MALLET
 */

#include "character.h"

/**
 * @brief L'ordre du type des adversaires qu'on affronte
 *
 */

int charac_ordre[4] = {1, 2, 3, 4};

/**
 * @brief Nombre de character que le joueur affronte
 *
 */
int nbr_charac = 4;

/**
 * @brief Paramétres pour l'initialisation
 *
 * Chaque ligne représente un type
 * Les colonnes représente successivement :
 *          attack, defense, hp, mp, evade, h_pot
 *
 * L'evasion doit être en 0 et 1000
 */

int init_param[5][6] = {{40, 10, 100, 100, 300, 5},
                        {25, 10, 125, 100, 300, 5},
                        {20, 10, 100, 40, 50, 5},
                        {15, 20, 150, 150, 100, 15},
                        {30, 5, 90, 200, 500, 10}};

/**
 * @brief Les hp maximum de chaque type
 *
 */

int hp_max[5] = {100, 125, 100, 150, 90};

/**
 * @brief Matrices de Markhov des types
 *
 * Dans l'ordre correspond au Legionnaire, berserker, paladins, archer
 *
 * Dans l'ordre : attack_phy, attack_mag, def, heal
 * les 4 suivantes correspondent à quand la vie est inferieur à 30% de la vie
 * max
 *
 */

int tab[4][8][4] = {{{25, 25, 25, 25},
                     {25, 25, 25, 25},
                     {20, 10, 60, 10},
                     {33, 33, 33, 34},
                     {15, 15, 15, 55},
                     {15, 15, 15, 55},
                     {30, 30, 10, 30},
                     {30, 30, 30, 10}},
                    {{60, 0, 30, 10},
                     {60, 0, 30, 10},
                     {90, 0, 0, 10},
                     {60, 0, 30, 10},
                     {60, 0, 30, 10},
                     {60, 0, 30, 10},
                     {30, 0, 60, 10},
                     {60, 0, 30, 10}},
                    {{20, 20, 35, 25},
                     {20, 20, 35, 25},
                     {25, 25, 25, 25},
                     {25, 25, 25, 25},
                     {15, 15, 15, 55},
                     {15, 15, 15, 55},
                     {15, 15, 15, 55},
                     {15, 15, 15, 55}},
                    {{15, 35, 25, 35},
                     {15, 35, 25, 35},
                     {40, 10, 15, 35},
                     {25, 25, 25, 35},
                     {15, 15, 10, 60},
                     {15, 15, 10, 60},
                     {30, 10, 10, 50},
                     {15, 15, 10, 50}}};

void init_charac(entity_charac_t *p, TYPE type) {
  p->type = type;
  p->action = ATT_PHY;
  p->bonus.attack = 0;
  p->bonus.defense = 0;
  p->bonus.evade = 0;
  p->stat.attack = init_param[type][0];
  p->stat.defense = init_param[type][1];
  p->stat.evade = init_param[type][4];
  p->hp = init_param[type][2];
  p->mp = init_param[type][3];
  p->nbr_pot = init_param[type][5];
}

void markhov(entity_charac_t *p1, entity_charac_t *p2, ACTION *act2) {
  ACTION i = 0;
  int vie = 0;
  int sum = 0;
  int norm = 100;
  int list[4] = {1, 1, 1, 1};

  if (p2->hp < 0.3 * hp_max[p2->type - 1]) {
    vie = 1;
  }
  if(p2->mp<MP_COST){         //si on a plus de mana
    norm -= tab[p2->type-1][p1->action + 4 * vie][1];
    list[1] = 0;  
  }
  if(p2->nbr_pot<1){          //si on a plus de pote
    norm -= tab[p2->type-1][p1->action + 4 * vie][3];
    list[3] = 0;
  }

  int r = rand() % 100;

  for (i = 0; i < 4; i++){
    if(list[i]){
      sum += 100*tab[p2->type-1][vie*4+p1->action][i]/norm;
    }
    if(r<sum){
      *act2 = i;
      break;
    }
  }
}

//affécté par les shields
int att_phy(entity_charac_t *sender, entity_charac_t *receiver, ACTION actr) {
  int val = 0;
  int tot_attack = sender->stat.attack + sender->bonus.attack;
  int tot_def = receiver->stat.defense + receiver->bonus.defense;
  int tot_evade = receiver->stat.evade + receiver->bonus.evade;

  int r = rand() % 1001;

  if (tot_evade < r) {
    receiver->hp -= (tot_attack - tot_def > 0) ? tot_attack - tot_def : 0;
    if (actr == SHIELD) {
      val = 1;
    }
  } else {
    val = 2;
  }
  return val;
}

//pas affécté par les shields
int att_mag(entity_charac_t *sender, entity_charac_t *receiver) {

  int val = 0;
  int tot_attack = sender->stat.attack / 2 + sender->bonus.attack / 2;
  int tot_evade = receiver->stat.evade + receiver->bonus.evade;

  int r = rand() % 1001;

  if (sender->mp >= MP_COST) {
    if (tot_evade < r) {
      receiver->hp -= tot_attack;
      sender->mp -= MP_COST;
    } else {
      val = 2;
    }
  } else {
    val = -1;
  }

  return val;
}

int def(entity_charac_t * receiver){
    receiver->bonus.defense += receiver->stat.defense;
    return 0;
}

int heal(entity_charac_t * receiver){
    int val = 0;

    if(receiver->nbr_pot>0){
        receiver->nbr_pot -= 1;
        receiver->hp = (receiver->hp+POT_HEAL_VAL>hp_max[receiver->type])?hp_max[receiver->type]:receiver->hp+POT_HEAL_VAL;
    }
    else{
        val = -1;
    }

    return val;
}

void update_bonus(entity_charac_t *p) {
  if (p->action == SHIELD) {
    p->bonus.defense -= p->stat.defense;
  }
  if (p->type == BERSERKER) {
    p->bonus.attack = (hp_max[p->type] - p->hp) / 2;
  }
}
