/**
 * @file environnement.h
 * @author Antoine Aroles (antoine.aroles@etu.isima.fr)
 * @brief prototype des fonctions d'environnement
 * @date 2022-06-22
 *
 */

#ifndef __ENVIRONNEMENT_H__
#define __ENVIRONNEMENT_H__

#include "UI.h"
#include "character.h"
#include "sdl.h"

void playRound(entity_charac_t *, entity_charac_t *, ACTION, ACTION,
               graphique_t *, texture_t *);

#endif
