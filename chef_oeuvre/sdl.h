/**
 *   \file sdl.h
 *   \brief le header de la partie graphique du chef d'oeuvre
 *
 *  Utilise aussi le fichier UI.c
 *
 */

#ifndef SDL_H
#define SDL_H

#include "UI.h"
#include "character.h"

/**
 *
 * \struct texture_t
 * \brief texture contenant la plupart des textures et des rect
 *
 * Permet d'éviter d'avoirs plus de 5 arguments par fonction, évite aussi de
 * recalculer à chaque fois
 *
 **/

typedef struct {

  SDL_Texture *easy;
  SDL_Texture *hard;

  SDL_Texture *sol;
  SDL_Rect rect_sol;

  SDL_Texture *ciel;
  SDL_Rect rect_ciel;

  SDL_Texture *player;
  SDL_Texture *ennemy;

  SDL_Texture *butAttP;
  SDL_Texture *butAttM;
  SDL_Texture *butDef;
  SDL_Texture *butHeal;

  SDL_Texture *looseT;
  SDL_Texture *winT;
  SDL_Rect positionCharacter[2];

  SDL_Texture *healT;
  SDL_Rect rectHealSize;

  SDL_Texture *shield;
  SDL_Rect rectShieldInSprite;

  SDL_Texture *mag;
  SDL_Rect rectMagSize;

  SDL_Texture *parade;
  SDL_Rect rectParadeSize;

  SDL_Texture *dodge;
  SDL_Rect rectDodgeSize;

  SDL_Texture *phy;
  SDL_Rect rectPhySize;
  SDL_Rect rectButton;

  SDL_Texture *popo;

} texture_t;

/**
 *
 * \fn void initTexture(graphique_t * graph, texture_t *T)
 * \brief initialise toutes les textures
 * \param graphique_t * graph: le graphique de ce projet
 * \param texture_t *T: les textures à initialiser
 * \return Null
 *
 **/

void initTexture(graphique_t *graph, texture_t *T);

/**
 *
 * \fn void initCharacter(graphique_t * graph, texture_t *T)
 * \brief initialise les textures du personnages type
 * \param graphique_t * graph: le graphique de ce projet
 * \param texture_t *T: les textures à initialiser
 * \param int type: le type de personnage
 * \return Null
 *
 **/

void initCharacter(graphique_t *graph, int type, texture_t *T);

/**
 *
 * \fn void modifyAll(graphique_t *graph, texture_t *T)
 * \brief modifie la position de la plupart des SDL_Rect lorsque la fenêtre est
 *redimensionner
 * \param graphique_t * graph: le graphique de ce projet
 * \param texture_t *T: les textures utilisés
 * \return Null
 *
 **/

void modifyAll(graphique_t *graph, texture_t *T);

/**
 *
 * \fn void effectPhy(graphique_t *graph,int position,int compteur texture_t *T)
 * \brief animation d'attaque physique
 * \param graphique_t * graph: le graphique de ce projet
 * \param int position: la position de l'animation (en fonction du joueur)
 * \param int compteur: compteur pour savoir l'avancement dans notre animation
 * \param texture_t *T: les textures utilisés
 * \return Null
 *
 **/
void effectPhy(graphique_t *graph, int position, int compteur, texture_t *T);

/**
 *
 * \fn void effectMag(graphique_t *graph,int position,int compteur
 * texture_t* T)
 *\brief animation d'attaque magique
 *\param graphique_t * graph: le graphique de ce projet
 * \param int position: la position de l'animation (en fonction du joueur)
 * \param int compteur: compteur pour savoir l'avancement dans notre animation
 * \param texture_t *T: les textures utilisés
 * \return Null
 *
 **/

void effectMag(graphique_t *graph, int position, int compteur, texture_t *T);

/**
 *
 * \fn void effectDefence(graphique_t *graph,int position,int compteur
 * texture_t* T)
 *\brief animation de défense
 *\param graphique_t * graph: le graphique de ce projet
 * \param int position: la position de l'animation (en fonction du joueur)
 * \param int compteur: compteur pour savoir l'avancement dans notre animation
 * \param texture_t *T: les textures utilisés
 * \return Null
 *
 **/

void effectDefence(graphique_t *graph, int position, int compteur,
                   texture_t *T);

/**
 *
 * \fn void effectHeal(graphique_t *graph,int position,int compteur
 * texture_t* T)
 *\brief animation de soin
 *\param graphique_t * graph: le graphique de ce projet
 * \param int position: la position de l'animation (en fonction du joueur)
 * \param int compteur: compteur pour savoir l'avancement dans notre animation
 * \param texture_t *T: les textures utilisés
 * \return Null
 *
 **/

void effectHeal(graphique_t *graph, int position, int compteur, texture_t *T);

/**
 *
 * \fn void effectDodge(graphique_t *graph,int position,int compteur
 * texture_t* T)
 *\brief animation d'esquive
 *\param graphique_t * graph: le graphique de ce projet
 * \param int position: la position de l'animation (en fonction du joueur)
 * \param int compteur: compteur pour savoir l'avancement dans notre animation
 * \param texture_t *T: les textures utilisés
 * \return Null
 *
 **/

void effectDodge(graphique_t *graph, int position, int compteur, texture_t *T);

/**
 *
 * \fn void effectParade(graphique_t *graph,int position,int compteur
 * texture_t* T)
 *\brief animation de parade
 *\param graphique_t * graph: le graphique de ce projet
 * \param int position: la position de l'animation (en fonction du joueur)
 * \param int compteur: compteur pour savoir l'avancement dans notre animation
 * \param texture_t *T: les textures utilisés
 * \return Null
 *
 **/

void effectParade(graphique_t *graph, int position, int compteur, texture_t *T);

/**
 *
 * \fn void drawScenery(graphique_t *graph, texture_t *T)
 * \brief reprèsente graphiquement le décor du jeu
 * \param graphique_t * graph: le graphique de ce projet
 * \param texture_t *T: les textures utilisés
 * \return Null
 *
 **/

void drawScenery(graphique_t *graph, texture_t *T);

/**
 *
 * \fn void drawCharacter(graphique_t *graph, texture_t *T)
 * \brief reprèsente graphiquement les personnages du jeu
 * \param graphique_t * graph: le graphique de ce projet
 * \param texture_t *T: les textures utilisés
 * \return Null
 *
 **/

void drawCharacter(graphique_t *graph, texture_t *T);

/**
 *
 * \fn void drawBoutonChoix(graphique_t *graph, texture_t *T)
 * \brief reprèsente graphiquement les boutons d'actions du jeu
 * \param graphique_t * graph: le graphique de ce projet
 * \param texture_t *T: les textures utilisés
 * \return Null
 *
 **/

void drawBoutonChoix(graphique_t *graph, int mp_cost, int mp, int nbr_pot,
                     texture_t *T);

/**
 *
 * \fn void drawAcc(graphique_t *graph, texture_t *T)
 * \brief reprèsente graphiquement le menu d'accueil du jeu
 * \param graphique_t * graph: le graphique de ce projet
 * \param texture_t *T: les textures utilisés
 * \return Null
 *
 **/

void drawAcc(graphique_t *graph, texture_t *T);

/**
 *
 * \fn void drawLifeBar(graphique_t *graph, entity_charac_t p1, entity_charac_t
 * p2, int current_charac, texture_t *T)
 * \brief reprèsente graphiquement les barres de vie des personnages
 * \param graphique_t * graph: le graphique de ce projet
 * \param texture_t *T: les textures utilisés
 * \param entity_charac_t p1: joueur1
 * \param entity_charac_t p2: joueur2
 * \param int current_charac: le type de l'adversaire actuelle
 * \return Null
 *
 **/

void drawLifeBar(graphique_t *graph, entity_charac_t p1, entity_charac_t p2,
                 int current_charac, texture_t *T);

/**
 *
 * \fn void drawWin(graphique_t *graph, texture_t *T)
 * \brief reprèsente graphiquement l'écran de victoire
 * \param graphique_t * graph: le graphique de ce projet
 * \param texture_t *T: les textures utilisés
 * \return Null
 *
 **/

void drawWin(graphique_t *graph, texture_t *T);

/**
 *
 * \fn void drawLoose(graphique_t *graph, texture_t *T)
 * \brief reprèsente graphiquement l'écran de défaite
 * \param graphique_t * graph: le graphique de ce projet
 * \param texture_t *T: les textures utilisés
 * \return Null
 *
 **/

void drawLoose(graphique_t *graph, texture_t *T);

/**
 *
 * \fn void drawHealthPotion(graphique_t *graph, int nbr_pot, int position,
                      texture_t *T)
 * \brief reprèsente graphiquement le nombre de potion restante pour un
 * personnage
 * \param graphique_t * graph: le graphique de ce projet
 * \param int nbr_pot: le nombre de potion
 * \param int position: la position du personnage (0 si joueur, 1 sinon)
 * \param texture_t *T: les textures utilisés
 * \return Null
 *
 **/

void drawHealthPotion(graphique_t *graph, int nbr_pot, int position,
                      texture_t *T);

/**
 *
 * \fn void drawManaBar(graphique_t *graph, entity_charac_t p1, entity_charac_t
 * p2, int current_charac, texture_t *T)
 * \brief reprèsente graphiquement les barres de magie des personnages
 * \param graphique_t * graph: le graphique de ce projet
 * \param entity_charac_t p1: joueur1
 * \param entity_charac_t p2: joueur2
 * \param int current_charac: le type de l'adversaire actuelle
 * \param texture_t *T: les textures utilisés
 * \return Null
 *
 **/

void drawManaBar(graphique_t *graph, entity_charac_t p1, entity_charac_t p2,
                 int current_charac, texture_t *T);

/**
 *
 * \fn void freeTexture(texture_t *T)
 * \brief libère toute nos textures
 * \param texture_t *T: les textures utilisés
 * \return Null
 *
 **/

void freeTexture(texture_t *T);

#endif /* SDL_H */
