/**
 * @file environnement.c
 * @author Antoine Aroles (antoine.aroles@etu.isima.fr)
 * @brief defintion des fonctions d'environnement
 * @date 2022-06-22
 *
 */

#include "environnement.h"
#include "sdl.h"

void playRound(entity_charac_t *c1, entity_charac_t *c2, ACTION act1,
               ACTION act2, graphique_t *graph, texture_t *T) {

  int time;
  void (*pf1)(graphique_t *, int, int, texture_t *) = NULL;
  void (*pf2)(graphique_t *, int, int, texture_t *) = NULL;
  void (*pf3)(graphique_t *, int, int, texture_t *) = NULL;
  void (*pf4)(graphique_t *, int, int, texture_t *) = NULL;

  /* Partie logique */

  entity_charac_t *characters[] = {c1, c2};
  ACTION actions[] = {act1, act2};
  int results[2] = {0, 0}; // tableau de retour des actions
  int order[2] = {
      0}; // tableau contenant l'ordre dans lequel les actions vont s'effectuer
  if (act2 > act1)
    order[0] = 1;
  else
    order[1] = 1;

  int i, j, k;
  for (i = 0; i < 2; i++) {
    j = order[i];           // indice dans les tableau de l'action en cours
    k = order[(i + 1) % 2]; // indice dans les tableaux de l'autre action
    switch (actions[j]) {
    case ATT_PHY:
      results[j] = att_phy(characters[j], characters[k], actions[k]);
      break;

    case ATT_MAG:
      results[j] = att_mag(characters[j], characters[k]);
      break;

    case SHIELD:
      results[j] = def(characters[j]);
      break;

    case H_POT:
      results[j] = heal(characters[j]);
      break;
    }

    c1->action = act1;
    c2->action = act2;
  }

  /* Partie graphique */
  switch (act1) {
  case ATT_PHY:
    pf1 = effectPhy;
    break;
  case ATT_MAG:
    pf1 = effectMag;
    break;
  case SHIELD:
    pf1 = effectDefence;
    break;
  case H_POT:
    pf1 = effectHeal;
    break;
  }
  switch (act2) {
  case ATT_PHY:
    pf2 = effectPhy;
    break;
  case ATT_MAG:
    pf2 = effectMag;
    break;
  case SHIELD:
    pf2 = effectDefence;
    break;
  case H_POT:
    pf2 = effectHeal;
    break;
  }
  if (results[0]) {
    switch (results[0]) {
    case 1:
      pf3 = effectParade;
      break;
    case 2:
      pf3 = effectDodge;
      break;
    default:
      pf3 = NULL;
      break;
    }
  }
  if (results[1]) {
    switch (results[1]) {
    case 1:
      pf4 = effectParade;
      break;
    case 2:
      pf4 = effectDodge;
      break;
    default:
      pf4 = NULL;
      break;
    }
  }

  c1->action = act1;
  c2->action = act2;
  update_bonus(c2);
  update_bonus(c1);

  for (time = 0; time < 30; time++) {
    drawScenery(graph, T);
    drawCharacter(graph, T);

    pf1(graph, 0, time, T);
    pf2(graph, 1, time, T);
    if (pf3) {
      pf3(graph, 1, time, T);
    }
    if (pf4) {
      pf4(graph, 0, time, T);
    }
    SDL_RenderPresent(graph->rend);
    SDL_Delay(40);
  }
}
