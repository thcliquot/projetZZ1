/**
 *
 * \file sld.c
 * \brief regroupe la partie graphique du chef d'oeuvre
 * \author Mallet Aroles Cliquot
 *
 * Voir le .h pour plus d'information
 *
 **/

#include "sdl.h"
// Ce serait mieux si sdl.c ne dépend pas de character.h,.
#include "character.h"
#include <time.h>

extern int init_param[5][6];

double rotation = 0;

/*================================================================
  |                                                              |
  |                           MODIFY                             |
  |                                                              |
  ================================================================*/

void modifyCharacterPosition(graphique_t *graph, texture_t *T) {
  T->positionCharacter[0].x = (graph->width - T->positionCharacter[0].w) / 4;
  T->positionCharacter[1].x =
      3 * (graph->width - T->positionCharacter[1].w) / 4;
  T->positionCharacter[0].y =
      4 * (graph->height - T->positionCharacter[0].h) / 5;
  T->positionCharacter[1].y =
      4 * (graph->height - T->positionCharacter[1].h) / 5;
  // positionCharacter[1].w = graph->width / 5;
  // positionCharacter[0].w = graph->width / 5;
  // positionCharacter[1].h = graph->height / 5;
  // positionCharacter[0].h = graph->height / 5;
}

void modifyGroundPosition(graphique_t *graph, texture_t *T) {
  T->rect_sol.x = 0;
  T->rect_sol.y = graph->height / 4;
  T->rect_sol.w = graph->width;
  T->rect_sol.h = graph->height;
}

void modifySkyPosition(graphique_t *graph, texture_t *T) {
  int offset = graph->height / 2;
  T->rect_ciel.w = graph->width * 100 / 70;
  T->rect_ciel.h = (offset + graph->height) * 100 / 70;
  T->rect_ciel.w = T->rect_ciel.h =
      T->rect_ciel.w > T->rect_ciel.h ? T->rect_ciel.w : T->rect_ciel.h;

  T->rect_ciel.x = (graph->width - T->rect_ciel.w) / 2;
  T->rect_ciel.y = (offset + graph->height - T->rect_ciel.h) / 2;
}

void modifyAll(graphique_t *graph, texture_t *T) {
  modifyGroundPosition(graph, T);
  modifySkyPosition(graph, T);
  modifyCharacterPosition(graph, T);
}

/*================================================================
  |                                                              |
  |                       EFFECT + DRAW                          |
  |                                                              |
  ================================================================*/

void effectDefence(graphique_t *graph, int position, int compteur,
                   texture_t *T) {
  if (compteur > 8)
    compteur = 8;
  T->rectShieldInSprite.x = T->rectShieldInSprite.w * (compteur % 2);
  T->rectShieldInSprite.y = T->rectShieldInSprite.h * (compteur / 2);

  SDL_RenderCopy(graph->rend, T->shield, &T->rectShieldInSprite,
                 &T->positionCharacter[position]);
}

void effectHeal(graphique_t *graph, int position, int compteur, texture_t *T) {

  if (compteur % 5 == 0) {
    T->rectHealSize.x = rand() % T->positionCharacter[position].w +
                        T->positionCharacter[position].x;
    T->rectHealSize.y = rand() % (T->positionCharacter[position].h) +
                        T->positionCharacter[position].y;
  }
  SDL_RenderCopy(graph->rend, T->healT, NULL, &T->rectHealSize);
}

void effectMag(graphique_t *graph, int position, int compteur, texture_t *T) {
  int deb = T->positionCharacter[position].x +
            (position + 1) % 2 * T->positionCharacter[position].w / 2;
  int fin = T->positionCharacter[(position + 1) % 2].x +
            position * T->positionCharacter[position].w / 3;
  int pas = (fin - deb) / 20;

  if (compteur < 20) {
    T->rectMagSize.x = deb + compteur * pas;
    T->rectMagSize.y =
        T->positionCharacter[0].y + T->positionCharacter[position].h / 4;
    SDL_RenderCopyEx(graph->rend, T->mag, NULL, &T->rectMagSize,
                     compteur * 360 / 20, NULL, SDL_FLIP_NONE);
  }
}

void effectParade(graphique_t *graph, int position, int compteur,
                  texture_t *T) {
  SDL_Rect src = {compteur % 10 * 100, 0, 100, 100};
  if ((compteur < 30) && (compteur > 20)) {
    T->rectParadeSize.x = T->positionCharacter[position].x;
    T->rectParadeSize.y = T->positionCharacter[position].y;
    T->rectParadeSize.w = T->positionCharacter[position].w;
    T->rectParadeSize.h = T->positionCharacter[position].h;
    SDL_RenderCopy(graph->rend, T->parade, &src, &T->rectParadeSize);
  }
};

void effectDodge(graphique_t *graph, int position, int compteur, texture_t *T) {
  SDL_Rect src = {compteur % 10 * 100, 0, 100, 100};
  if ((compteur < 30) && (compteur > 20)) {
    T->rectDodgeSize.x = T->positionCharacter[position].x;
    T->rectDodgeSize.y = T->positionCharacter[position].y;
    T->rectDodgeSize.w = T->positionCharacter[position].w;
    T->rectDodgeSize.h = T->positionCharacter[position].h;
    SDL_RenderCopy(graph->rend, T->dodge, &src, &T->rectDodgeSize);
  }
};

void effectPhy(graphique_t *graph, int position, int compteur, texture_t *T) {
  int deb = T->positionCharacter[position].x +
            (position + 1) % 2 * T->positionCharacter[position].w / 2;
  int fin = T->positionCharacter[(position + 1) % 2].x +
            position * T->positionCharacter[position].w / 3;
  int pas = (fin - deb) / 20;

  if (compteur < 20) {
    T->rectPhySize.x = deb + compteur * pas;
    T->rectPhySize.y =
        T->positionCharacter[0].y + T->positionCharacter[position].h / 4;
    SDL_RenderCopyEx(graph->rend, T->phy, NULL, &T->rectPhySize,
                     compteur * 360 / 20, NULL, SDL_FLIP_NONE);
  }
}

void drawScenery(graphique_t *graph, texture_t *T) {

  SDL_RenderCopyEx(graph->rend, T->ciel, NULL, &T->rect_ciel, rotation, NULL,
                   SDL_FLIP_NONE);
  SDL_RenderCopy(graph->rend, T->sol, NULL, &T->rect_sol);

  rotation += 0.1;
}

void drawCharacter(graphique_t *graph, texture_t *T) {

  SDL_RenderCopyEx(graph->rend, T->player, NULL, &T->positionCharacter[0], 0,
                   NULL, SDL_FLIP_NONE);
  SDL_RenderCopyEx(graph->rend, T->ennemy, NULL, &T->positionCharacter[1], 0,
                   NULL, SDL_FLIP_HORIZONTAL);
}

void drawBoutonChoix(graphique_t *graph, int mp_cost, int mp, int nbr_pot,
                     texture_t *T) {
  T->rectButton.y = 9 * graph->height / 10;
  T->rectButton.x = graph->width / 8 - T->rectButton.w / 2;
  SDL_RenderCopy(graph->rend, T->butAttP, NULL, &T->rectButton);

  if (mp > mp_cost) {
    T->rectButton.x = 3 * graph->width / 8 - T->rectButton.w / 2;
    SDL_RenderCopy(graph->rend, T->butAttM, NULL, &T->rectButton);
  }

  T->rectButton.x = 5 * graph->width / 8 - T->rectButton.w / 2;
  SDL_RenderCopy(graph->rend, T->butDef, NULL, &T->rectButton);

  if (nbr_pot > 0) {
    T->rectButton.x = 7 * graph->width / 8 - T->rectButton.w / 2;
    SDL_RenderCopy(graph->rend, T->butHeal, NULL, &T->rectButton);
  }
}

void drawAcc(graphique_t *graph, texture_t *T) {
  SDL_SetRenderDrawColor(graph->rend, 0, 0, 0, 255);

  SDL_Rect btn1;
  SDL_Rect btn2;

  btn1.x = graph->height / 8;
  btn1.y = 5 * (graph->height / 8);
  btn1.w = graph->width / 4;
  btn1.h = graph->height / 8;

  btn2.x = 5 * (graph->width / 8);
  btn2.y = 5 * (graph->height / 8);
  btn2.w = graph->width / 4;
  btn2.h = graph->height / 8;

  SDL_RenderCopy(graph->rend, T->easy, NULL, &btn1);
  SDL_RenderCopy(graph->rend, T->hard, NULL, &btn2);
}

// A revoir, utilise character.h
void drawLifeBar(graphique_t *graph, entity_charac_t p1, entity_charac_t p2,
                 int current_charac, texture_t *T) {
  entity_charac_t charac[] = {p1, p2};
  SDL_Rect bg[2];
  SDL_Rect fg[2];

  for (int i = 0; i < 2; i++) {
    bg[i].w = (graph->width * init_param[(current_charac + 1) * i][2]) / 600;
    bg[i].h = graph->height / 20;
    fg[i].w = (graph->width * charac[i].hp) / 600;
    fg[i].h = graph->height / 20;

    bg[i].x = T->positionCharacter[i].x;
    bg[i].y = T->positionCharacter[i].y - (graph->height) / 20;
    fg[i].x = T->positionCharacter[i].x;
    fg[i].y = T->positionCharacter[i].y - (graph->height) / 20;
  }

  SDL_SetRenderDrawColor(graph->rend, 175, 50, 20, 255);
  SDL_RenderFillRect(graph->rend, &bg[0]);
  SDL_RenderFillRect(graph->rend, &bg[1]);

  SDL_SetRenderDrawColor(graph->rend, 50, 175, 20, 255);
  SDL_RenderFillRect(graph->rend, &fg[0]);
  SDL_RenderFillRect(graph->rend, &fg[1]);
}

// A revoir, utilise character.h
void drawManaBar(graphique_t *graph, entity_charac_t p1, entity_charac_t p2,
                 int current_charac, texture_t *T) {
  entity_charac_t charac[] = {p1, p2};
  SDL_Rect bg[2];
  SDL_Rect fg[2];

  for (int i = 0; i < 2; i++) {
    bg[i].w = (graph->width * init_param[(current_charac + 1) * i][3]) / 600;
    bg[i].h = graph->height / 20;
    fg[i].w = (graph->width * charac[i].mp) / 600;
    fg[i].h = graph->height / 20;

    bg[i].x = T->positionCharacter[i].x;
    bg[i].y = T->positionCharacter[i].y - (graph->height) / 10;
    fg[i].x = T->positionCharacter[i].x;
    fg[i].y = T->positionCharacter[i].y - (graph->height) / 10;
  }

  SDL_SetRenderDrawColor(graph->rend, 75, 75, 75, 255);
  SDL_RenderFillRect(graph->rend, &bg[0]);
  SDL_RenderFillRect(graph->rend, &bg[1]);

  SDL_SetRenderDrawColor(graph->rend, 20, 50, 175, 255);
  SDL_RenderFillRect(graph->rend, &fg[0]);
  SDL_RenderFillRect(graph->rend, &fg[1]);
}

void drawWin(graphique_t *graph, texture_t *T) {

  SDL_Rect rect;

  SDL_QueryTexture(T->winT, NULL, NULL, &rect.w, &rect.h);

  rect.w = (graph->width + rect.w / 2) / 2;
  rect.h = (graph->height + rect.h / 2) / 2;
  rect.x = (graph->width - rect.w) / 2;
  rect.y = (graph->height - rect.h) / 2;

  SDL_RenderCopy(graph->rend, T->winT, NULL, &rect);
}

void drawLoose(graphique_t *graph, texture_t *T) {
  SDL_Rect rect;

  SDL_QueryTexture(T->looseT, NULL, NULL, &rect.w, &rect.h);

  rect.w = (graph->width + rect.w / 2) / 2;
  rect.h = (graph->height + rect.h / 2) / 2;
  rect.x = (graph->width - rect.w) / 2;
  rect.y = (graph->height - rect.h) / 2;

  SDL_RenderCopy(graph->rend, T->looseT, NULL, &rect);
}

void drawHealthPotion(graphique_t *graph, int nbr_pot, int position,
                      texture_t *T) {

  SDL_Rect rect;
  SDL_QueryTexture(T->popo, NULL, NULL, &rect.w, &rect.h);

  rect.x = T->positionCharacter[position].x +
           T->positionCharacter[position].w / 2 + rect.w * 2;
  rect.y = T->positionCharacter[position].y;

  for (int i = 0; i < nbr_pot; i++) {
    SDL_RenderCopy(graph->rend, T->popo, NULL, &rect);
    rect.y += 10;
  }
}

/*================================================================
  |                                                              |
  |                           INIT                               |
  |                                                              |
  ================================================================*/

void initCharacter(graphique_t *graph, int type, texture_t *T) {
  initialiserImage(&T->player, "img/player.png", graph->rend);
  switch (type) {
  case 1:
    initialiserImage(&T->ennemy, "img/legionnaire.png", graph->rend);
    break;
  case 2:
    initialiserImage(&T->ennemy, "img/berserker.png", graph->rend);
    break;
  case 3:
    initialiserImage(&T->ennemy, "img/paladin.png", graph->rend);
    break;
  case 4:
    initialiserImage(&T->ennemy, "img/archer.png", graph->rend);
    break;
  }
  SDL_QueryTexture(T->ennemy, NULL, NULL, &T->positionCharacter[1].w,
                   &T->positionCharacter[1].h);
  SDL_QueryTexture(T->player, NULL, NULL, &T->positionCharacter[0].w,
                   &T->positionCharacter[0].h);
  T->positionCharacter[0].w /= 1.5;
  T->positionCharacter[1].h /= 1.5;
  T->positionCharacter[0].h /= 1.5;
  T->positionCharacter[1].w /= 1.5;
}

void initScenery(graphique_t *graph, texture_t *T) {
  int iW, iH;
  initialiserImage(&T->sol, "img/ground.png", graph->rend);
  initialiserImage(&T->ciel, "img/sky.jpg", graph->rend);
  SDL_QueryTexture(T->ciel, NULL, NULL, &iW, &iH);
  T->rect_ciel.w = graph->width * 4;
  T->rect_ciel.h = graph->height * 4;
  T->rect_ciel.x = (graph->width - iW) / 2;
  T->rect_ciel.y = graph->height - iH / 2;
}

void initPhy(graphique_t *graph, texture_t *T) {
  initialiserImage(&T->phy, "img/sword.png", graph->rend);
  SDL_QueryTexture(T->phy, NULL, NULL, &T->rectPhySize.w, &T->rectPhySize.h);
  T->rectPhySize.h /= 1;
  T->rectPhySize.w /= 1;
}

void initDefence(graphique_t *graph, texture_t *T) {
  initialiserImage(&T->shield, "img/sheet_shield.png", graph->rend);
  T->rectShieldInSprite.w = T->rectShieldInSprite.h = 250;
  T->rectShieldInSprite.y = T->rectShieldInSprite.x = 0;
}
void initHeal(graphique_t *graph, texture_t *T) {
  initialiserImage(&T->healT, "img/heal.png", graph->rend);
  SDL_QueryTexture(T->healT, NULL, NULL, &T->rectHealSize.w,
                   &T->rectHealSize.h);
  T->rectHealSize.h /= 10;
  T->rectHealSize.w /= 10;
}
void initMag(graphique_t *graph, texture_t *T) {
  initialiserImage(&T->mag, "img/boule_feu.png", graph->rend);
  SDL_QueryTexture(T->mag, NULL, NULL, &T->rectMagSize.w, &T->rectMagSize.h);
  T->rectMagSize.h /= 1;
  T->rectMagSize.w /= 1;
}
void initParade(graphique_t *graph, texture_t *T) {
  initialiserImage(&T->parade, "img/parade.png", graph->rend);
}
void initDodge(graphique_t *graph, texture_t *T) {
  initialiserImage(&T->dodge, "img/dodge.png", graph->rend);
}
void initBoutonChoix(graphique_t *graph, texture_t *T) {
  initialiserImage(&T->butAttP, "img/but_attp.png", graph->rend);
  initialiserImage(&T->butAttM, "img/but_attm.png", graph->rend);
  initialiserImage(&T->butDef, "img/but_def.png", graph->rend);
  initialiserImage(&T->butHeal, "img/but_soin.png", graph->rend);
  SDL_QueryTexture(T->butAttP, NULL, NULL, &T->rectButton.w, &T->rectButton.h);
}
void initLoose(graphique_t *graph, texture_t *T) {
  initialiserImage(&T->winT, "img/win.png", graph->rend);
}
void initWin(graphique_t *graph, texture_t *T) {
  initialiserImage(&T->looseT, "img/loose.png", graph->rend);
}

void initHealPotion(graphique_t *graph, texture_t *T) {
  initialiserImage(&T->popo, "img/pot_heal.png", graph->rend);
}

void initButMenu(graphique_t *graph, texture_t *T) {
  initialiserImage(&T->easy, "./img/but_easy.png", graph->rend);
  initialiserImage(&T->hard, "./img/but_hard.png", graph->rend);
}
void initTexture(graphique_t *graph, texture_t *T) {
  initScenery(graph, T);

  modifyCharacterPosition(graph, T);
  modifySkyPosition(graph, T);
  modifyGroundPosition(graph, T);

  initBoutonChoix(graph, T);
  initDefence(graph, T);
  initHeal(graph, T);
  initParade(graph, T);
  initDodge(graph, T);
  initMag(graph, T);
  initPhy(graph, T);
  initHealPotion(graph, T);
  initLoose(graph, T);
  initWin(graph, T);
  initButMenu(graph, T);
}

/*================================================================
  |                                                              |
  |                           FREE                               |
  |                                                              |
  ================================================================*/

void freeTexture(texture_t *T) {

  SDL_DestroyTexture(T->easy);
  SDL_DestroyTexture(T->hard);
  SDL_DestroyTexture(T->sol);
  SDL_DestroyTexture(T->ciel);
  SDL_DestroyTexture(T->player);
  SDL_DestroyTexture(T->ennemy);
  SDL_DestroyTexture(T->butAttP);
  SDL_DestroyTexture(T->butAttM);
  SDL_DestroyTexture(T->butDef);
  SDL_DestroyTexture(T->butHeal);
  SDL_DestroyTexture(T->looseT);
  SDL_DestroyTexture(T->winT);
  SDL_DestroyTexture(T->healT);
  SDL_DestroyTexture(T->shield);
  SDL_DestroyTexture(T->mag);
  SDL_DestroyTexture(T->parade);
  SDL_DestroyTexture(T->dodge);
  SDL_DestroyTexture(T->phy);
  SDL_DestroyTexture(T->popo);

  free(T);
}
