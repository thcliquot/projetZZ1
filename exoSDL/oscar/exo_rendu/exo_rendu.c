#include <SDL2/SDL.h>
#include <math.h>
#include <stdio.h>
#include <string.h>

typedef struct carre_t{
    int posx;           //position du CENTRE
    int posy;           //position du CENTRE
    float direcx;             
    float direcy;
    int width;          //demi largeur
    int height;         //demi hauteur
} carre_t;

#define SPEED 3


void end_sdl(char ok,                                               // fin normale : ok = 0 ; anormale ok = 1
             char const* msg,                                       // message à afficher
             SDL_Window* window,                                    // fenêtre à fermer
             SDL_Renderer* renderer) {                              // renderer à fermer
  char msg_formated[255];                                                         
  int l;                                                                          

  if (!ok) {                                                        // Affichage de ce qui ne va pas
    strncpy(msg_formated, msg, 250);                                              
    l = strlen(msg_formated);                                                     
    strcpy(msg_formated + l, " : %s\n");                                          

    SDL_Log(msg_formated, SDL_GetError());                                        
  }                                                                               

  if (renderer != NULL) {                                           // Destruction si nécessaire du renderer
    SDL_DestroyRenderer(renderer);                                  // Attention : on suppose que les NULL sont maintenus !!
    renderer = NULL;
  }
  if (window != NULL)   {                                           // Destruction si nécessaire de la fenêtre
    SDL_DestroyWindow(window);                                      // Attention : on suppose que les NULL sont maintenus !!
    window= NULL;
  }

  SDL_Quit();                                                                     

  if (!ok) {                                                        // On quitte si cela ne va pas                
    exit(EXIT_FAILURE);                                                           
  }                                                                               
}




void fond(SDL_Renderer * renderer, int w, int h){
    SDL_Rect rectangle;                                                             

    SDL_SetRenderDrawColor(renderer,                                                
                            17, 124, 236,                                  // mode Red, Green, Blue (tous dans 0..255)
                            255);                                      // 0 = transparent ; 255 = opaque
    rectangle.x = 0;                                                  // x haut gauche du rectangle
    rectangle.y = 0;                                                  // y haut gauche du rectangle
    rectangle.w = w;                                                // sa largeur (w = width)
    rectangle.h = h;                                                // sa hauteur (h = height)

    SDL_RenderFillRect(renderer, &rectangle);            
}





void draw(SDL_Renderer* renderer, int posx, int posy, int width, int height) {                                 // Je pense que vous allez faire moins laid :)

  SDL_Rect rectangle;                                                             

    SDL_SetRenderDrawColor(renderer,                                                
                            posx, posy, height,                                  // mode Red, Green, Blue (tous dans 0..255)
                            130);                                      // 0 = transparent ; 255 = opaque
    rectangle.x = posx;                                                  // x haut gauche du rectangle
    rectangle.y = posy;                                                  // y haut gauche du rectangle
    rectangle.w = width;                                                // sa largeur (w = width)
    rectangle.h = height;                                                // sa hauteur (h = height)

    SDL_RenderFillRect(renderer, &rectangle);                                   // x,y seconde extrémité

}



int main(int argc, char** argv) {
    
    int deltax, deltay;
    int wind_w, wind_h;
    int i, j;
    int nbr_carre = 5;
    carre_t tab[nbr_carre];
    tab[0].direcx = 0.5, tab[0].direcy=1, tab[0].posx = 0, tab[0].posy=0, tab[0].width=50, tab[0].height=50;
    tab[1].direcx = 1, tab[1].direcy=-0.7, tab[1].posx = 400, tab[1].posy=400, tab[1].width=25, tab[1].height=25;
    tab[2].direcx = -1, tab[2].direcy=-1, tab[2].posx = 200, tab[2].posy=500, tab[2].width=60, tab[2].height=30;
    tab[3].direcx = -0.8, tab[3].direcy= 0.9, tab[3].posx = 500, tab[3].posy=600, tab[3].width=10, tab[3].height=10;
    tab[4].direcx = 0, tab[4].direcy= 0, tab[4].posx = 600, tab[4].posy=200, tab[4].width=5, tab[4].height=20;

    SDL_Event event;
    SDL_bool prg_on = SDL_TRUE;

    (void)argc;
    (void)argv;

    SDL_Window* window = NULL;
    SDL_Renderer* renderer = NULL;

    SDL_DisplayMode screen;


    if (SDL_Init(SDL_INIT_VIDEO) != 0) end_sdl(0, "ERROR SDL INIT", window, renderer);

    SDL_GetCurrentDisplayMode(0, &screen);
    printf("Résolution écran\n\tw : %d\n\th : %d\n",
            screen.w, screen.h);

    /* Création de la fenêtre */
    window = SDL_CreateWindow("Premier dessin",
                                SDL_WINDOWPOS_CENTERED,
                                SDL_WINDOWPOS_CENTERED, screen.w * 0.66,
                                screen.h * 0.66,
                                SDL_WINDOW_OPENGL);
    if (window == NULL) end_sdl(0, "ERROR WINDOW CREATION", window, renderer);
    wind_w = screen.w*0.66;
    wind_h = screen.h*0.66;

    /* Création du renderer */
    renderer = SDL_CreateRenderer(window, -1,
                                    SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == NULL) end_sdl(0, "ERROR RENDERER CREATION", window, renderer);

    while(prg_on){

        while(SDL_PollEvent(&event)){                     //si la file d'évènements n'est pas vide : défiler l'élément en tête
                                                        // de file dans 'event'
            switch(event.type){                         // En fonction de la valeur du type de cet évènement
            case SDL_QUIT:                              // Un évènement simple, on a cliqué sur la x de la // fenêtre
                prg_on = SDL_FALSE;                     // Il est temps d'arrêter le programme
                break;
            default:                                    // L'évènement défilé ne nous intéresse pas
                break;
            }
        }

        fond(renderer, wind_w, wind_h);
        for(i=0;i<nbr_carre;i++){
            draw(renderer, tab[i].posx-tab[i].width, tab[i].posy-tab[i].height, 2*tab[i].width, 2*tab[i].height);          //on update le rectangle;
        }
        SDL_RenderPresent(renderer);                                     // affichage
        SDL_Delay(5);                                                  // Pause exprimée en ms

        for(i=0;i<nbr_carre;i++){
            tab[i].posx = tab[i].posx+SPEED*tab[i].direcx;  //on bouge dans la direction x
            tab[i].posy = tab[i].posy+SPEED*tab[i].direcy;  //on bouge dans la direction y
            if(tab[i].posx+tab[i].width>wind_w){                 //dépassement à droite
                tab[i].posx = wind_w-tab[i].width;
                tab[i].direcx = -tab[i].direcx;
            }
            if(tab[i].posx-tab[i].width<0){                          //dépassement à gauche
                tab[i].posx = 0+tab[i].width;
                tab[i].direcx = -tab[i].direcx;
            }
            if(tab[i].posy+tab[i].height>wind_h){                   //dépassement en bas
                tab[i].posy = wind_h-tab[i].height;
                tab[i].direcy = -tab[i].direcy;
            }
            if(tab[i].posy-tab[i].height<0){                          //dépassement en haut
                tab[i].posy = 0+tab[i].height;
                tab[i].direcy = -tab[i].direcy; 
            }
        }
        for(i=0;i<nbr_carre;i++){
            j=i+1;
            for(j=i+1;j<nbr_carre;j++){
                deltax = tab[i].posx-tab[j].posx;
                deltay = tab[i].posy-tab[j].posy;
                if(abs(deltay)-(tab[i].height+tab[j].height)<SPEED/2){
                    if(abs(deltax)-(tab[i].width+tab[j].width)<SPEED/2){
                        if(abs(deltax)-(tab[i].width+tab[j].width)>-3*SPEED){
                            tab[i].direcx = +fabs(tab[i].direcx)*(deltax)/abs(deltax);             //On mets à jour les directions
                            tab[j].direcx = -fabs(tab[j].direcx)*(deltax)/abs(deltax);             //à l'aide du sens de la collision
                        }
                        if(abs(deltay)-(tab[i].height+tab[j].height)>-3*SPEED){
                            tab[i].direcy = +fabs(tab[i].direcy)*(deltay)/abs(deltay);
                            tab[j].direcy = -fabs(tab[j].direcy)*(deltay)/abs(deltay);
                        }
                    }
                }
            }
        }
    }

    /* on referme proprement la SDL */
    end_sdl(1, "Normal ending", window, renderer);
    return EXIT_SUCCESS;
}