#include <SDL2/SDL.h>
#include <stdio.h>
#include <math.h>

/********************************************/
/* Vérification de l'installation de la SDL */
/********************************************/

#define NBR_WINDOW 20

void destroy_all_windows(int num_fin, SDL_Window **list){
    int i;

    for(i=0;i<=num_fin;i++){
        SDL_DestroyWindow(list[i]);
    }
}

void init_windows(SDL_Window **window_list){

    int i;

    for(i=0;i<NBR_WINDOW;i++){                    //boucle de création des fenêtres
        window_list[i] = SDL_CreateWindow(
            "Fenêtre",
            0, 0,
            100, 100,
            SDL_WINDOW_RESIZABLE);
    
        if (window_list[i] == NULL) {
        SDL_Log("Error : SDL window %d creation - %s\n", 
                i, SDL_GetError());              // échec de la création de la fenêtre
        SDL_Quit();                              // On referme la SDL    
        destroy_all_windows(i, window_list);     // On deétruit les autres fenêtres 
        exit(EXIT_FAILURE);
        }
    }
}


int main(int argc, char **argv) {

    int i = 0;
    float time = 0., pas = 0.5, pos;
    SDL_bool prg_on = SDL_TRUE;
    SDL_Event event;

    (void)argc;
    (void)argv;

    SDL_Window *window_list[NBR_WINDOW];

    /* Initialisation de la SDL  + gestion de l'échec possible */
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        SDL_Log("Error : SDL initialisation - %s\n", 
             SDL_GetError());                // l'initialisation de la SDL a échoué 
        exit(EXIT_FAILURE);
    }


    init_windows(window_list);

    while(prg_on){

        if (SDL_PollEvent(&event)){                     //si la file d'évènements n'est pas vide : défiler l'élément en tête
                                                        // de file dans 'event'
            switch(event.type){                         // En fonction de la valeur du type de cet évènement
            case SDL_KEYDOWN:                           //une touche a était préssé
                switch (event.key.keysym.sym) {         // la touche appuyée est ...
                case SDLK_SPACE:                        // 'SPC'
                    prg_on = SDL_FALSE;                 // on arrête le programme
                    break;
                default:                                // Une touche appuyée qu'on ne traite pas
                    break;
                }
                
                break;
            default:                                    // L'évènement défilé ne nous intéresse pas
                break;
            }
        }

        for(i=0;i<NBR_WINDOW;i++){                  //on calcule l'argument avant pour éviter de le faire deux fois
            pos = time+i*2*3.142857/NBR_WINDOW;
            SDL_SetWindowPosition(window_list[i],   //on change la position de la fenêtre i
                                  900+sin(time/10)*300*cos(pos),
                                  500+cos(time/25)*300*sin(pos)
                                 );
        }
        time = time+pas;
        SDL_Delay(20);
    }



    /* et on referme tout ce qu'on a ouvert en ordre inverse de la création */
    destroy_all_windows(NBR_WINDOW, window_list);      //on referme toutes les fenêtres

    SDL_Quit();                             
    return 0;
}

