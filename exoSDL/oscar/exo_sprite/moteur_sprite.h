
#ifndef moteur_sprite_h
#define moteur_sprite_h

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define G 1
#define SOL 420
#define STATE_NUM 4
#define TIMING 10

typedef struct pl_t{
    float direcx;
    float direcy;
    int posx;
    int posy;
    int state;
    int w;
    int h;
    int clock;
} pl_t;

typedef struct pressed_t{
    int press_q;
    int press_d;
    int press_spc;
} pressed_t;

void mouvement(pl_t *, pressed_t *);


#endif