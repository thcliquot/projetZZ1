#include "UI.h"
#include "moteur_sprite.h"


void eventFuncSprite(SDL_Event event, SDL_bool *running, pressed_t * press) {
  while (SDL_PollEvent(&event)) {
    switch (event.type) {
    case SDL_QUIT:
      *running = SDL_FALSE;
      break;
    case SDL_KEYDOWN:
        switch (event.key.keysym.sym){
        case SDLK_ESCAPE: 
            *running = SDL_FALSE;
            break;
        case SDLK_LEFT:
        case SDLK_q: // Déplacement gauche
            press->press_q = 1;
            break;
        case SDLK_RIGHT:
        case SDLK_d: // Déplacement droite
            press->press_d = 1;
            break;
        case SDLK_SPACE: // Déplacement haut
            press->press_spc = 1;
            break;
        default:
            break;
        }
        break;
    case SDL_KEYUP:
        switch (event.key.keysym.sym){
        case SDLK_ESCAPE: 
            *running = SDL_FALSE;
            break;
        case SDLK_LEFT:
        case SDLK_q: // Relachement gauche
            press->press_q = 0;
            break;
        case SDLK_RIGHT:
        case SDLK_d: // Relachement droite
            press->press_d = 0;
            break;
        default:
            break;
        }
    default:
        break;
    }
  }
}


void draw(SDL_Texture * img, char * path, SDL_Renderer * rend, pl_t * pl){
    SDL_RendererFlip flip = SDL_FLIP_NONE;
    int saut = 0;
    if(pl->state<0){                        //si on se deplache vers la droite
        flip = SDL_FLIP_HORIZONTAL;
    }
    if(pl->posy-(SOL-pl->h)<-1){            //si on est dans les air
        saut = 1;
    }
    SDL_Rect srcrect = {50*saut, 100*abs(pl->state), 45, 95};      //séléction du bout de l'image source
    SDL_Rect rect = {400, pl->posy, pl->w, pl->h};            //séléction de la taille de l'image
	initialiserImage(&img, path, rend);
    SDL_RenderCopyEx(rend, img, &srcrect, &rect, 0, NULL, flip);
}

void drawfond(SDL_Texture * img, char * path, SDL_Renderer * rend, pl_t * pl){
    SDL_Rect rect = {pl->posx, 0, 100, 380};
    initialiserImage(&img, path, rend);             
    SDL_RenderCopy(rend, img, &rect, NULL);
}


int main(){
    SDL_Event event;
    SDL_Window *win;
    SDL_Renderer *rend;
    SDL_bool running = SDL_TRUE;
    SDL_Texture * texture = NULL;

    int l = 940, L = 490;
    pressed_t press;                //pour stocker les touches qui ont était préssées
    press.press_d = 0; press.press_q = 0; press.press_spc = 0;
    pl_t pl;                        //personnage qu'on manipule
    pl.posx = 200; pl.posy = 300; pl.direcx = 0; pl.direcy = 0; pl.w = 50, pl.h = 100, pl.state = 0, pl.clock = 0;

    initialiserSDL();
    initialiserWindow(&win, 0, 0, l, L);
    initialiserRenderer(&rend, win);

    while(running){

        press.press_spc = 0;
        eventFuncSprite(event, &running, &press);                   //boucle d'événement

        mouvement(&pl, &press);                                     //appel au moteur du jeu pour gérer les mouvements

	    drawfond(texture, "./fond.png",rend, &pl);                    //rendu du fond 

        draw(texture, "./persom.png", rend, &pl);                    //rendu du sprite

        SDL_RenderPresent(rend);
        SDL_Delay(20);
    }
    return 0;
}