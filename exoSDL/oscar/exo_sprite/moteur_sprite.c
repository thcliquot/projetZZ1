#include "moteur_sprite.h"

void mouvement(pl_t * pl, pressed_t * press){
    
    if(press->press_d){
        pl->direcx += 5;
    }
    if(press->press_q){
        pl->direcx -= 5;
    }
    if(press->press_spc){
        if(pl->direcy==0){
            pl->state = -1;     //on remet à zéro car on démarre l'animation de saut (-1 car +1 juste après)
            pl->direcy = 20;
        }
    }
    if((pl->direcx)||(pl->direcy)){                         //si un mouvement
        if(pl->clock==TIMING){
            pl->state = (abs(pl->state))%(STATE_NUM-1)+1;       //on avance l'animation
        }
        pl->clock = (pl->clock+1)%(TIMING+1);               //on avance la clock qui ralentit les animations
    }else{
        pl->state = 0;
    }

    if(pl->direcx<0){
        pl->state = -1*abs(pl->state);              //on change le signe en fonction de la position
    }
    pl->posx = pl->posx + pl->direcx;

    if(pl->posx>560){                               //si on sort de la frame  
        pl->posx=560;
    }
    if(pl->posx<0){
        pl->posx=0;
    }
    pl->direcx = 0;
    
    pl->posy = pl->posy - pl->direcy;               //simulation de la gravité
    if(pl->posy-(SOL-pl->h)<0){
        pl->direcy = pl->direcy - G;
    }
    if(pl->posy-(SOL-pl->h)>0){                     //aterissage
        pl->posy = (SOL-pl->h);
        pl->direcy = 0;
    }

}