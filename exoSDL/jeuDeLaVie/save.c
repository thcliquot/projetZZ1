/**
 * @file save.c
 * @author Oscar M, Antoine A, Théo C.
 * @brief Fichier de load et save
 * @date 2022-06-22
 * 
 * Ce fichier contient les fonctions relative
 * au chargement avec un fichier
 * et à la sauvegarde d'un état dans un fichier.
 * 
 */

#include "save.h"
#include "moteur_jeu_vie.h"

/**
 * @brief fonction qui charge une matrice
 * 
 * @param file le fichier à charger
 * @param mot les paramétres du fichier (taille)
 */

void load(char *file, moteur_t *mot) {

  printf("Chargement en cours \n");
  FILE *f = fopen(file, "r");

  if (!fscanf(f, "%d\n%d\n", &mot->hauteur, &mot->largeur)) {
    fprintf(stderr, "INVALID READ\n");
    exit(-1);
  }
  mot->tabAct = myalloc2D(
      mot->hauteur + 2 * mot->type,
      mot->largeur + 2 * mot->type); // next_tab, tableau pour effectuer le changement
  mot->tabPrec = myalloc2D(
      mot->hauteur + 2 * mot->type,
      mot->largeur + 2 * mot->type); // tableau contenant les valeurs du jeu

  if (f != NULL) {
    for (int i = mot->type; i < mot->hauteur + mot->type; i++) {
      fgetc(f);
      for (int j = mot->type; j < mot->largeur + mot->type; j++) {
        mot->tabAct[i][j] = fgetc(f) - '0';
        fgetc(f);
      }
      fgetc(f);
    }

    fclose(f);
    printf("Chargement fini\n");
  } else {
    fprintf(stderr, "ERREUR FICHIER A CHARGER\n");
    exit(-1);
  }
}

/**
 * @brief Sauvegarde un état dans un fichier
 * 
 * @param world la matrice à sauvegarder
 * @param mot les paramétres du moteur (taille...)
 * 
 */

void save(int **world, moteur_t *mot) {
  int num = 0;
  char nomSauvegarde[20];
  FILE *f;

  printf("Sauvegarde en cours \n");

  // On crée le nom du fichier.
  sprintf(nomSauvegarde, "sauv_%d.txt", num);

  // On regarde si il existe déjà un fichier portant ce nom.
  while ((f = fopen(nomSauvegarde, "r"))) {
    // Si oui on le ferme et on incrémente le nom de notre fichier.
    fclose(f);
    num++;
    sprintf(nomSauvegarde, "sauv_%d.txt", num);
  }

  // Sinon, on écrit notre tableau du jeu de la vie dans ce fichier.
  f = fopen(nomSauvegarde, "w+");
  if (f == NULL) {
    perror("ERREUR CREATION FICHIER\n");
    exit(-1);
  }

  printf("Sauvegarde du jeu de la vie dans le fichier : %s\n", nomSauvegarde);

  fprintf(f, "%d\n%d\n", mot->hauteur, mot->largeur);

  for (int i = mot->type; i < mot->hauteur + mot->type; i++) {
    fputc('|', f);
    for (int j = mot->type; j < mot->largeur + mot->type; j++) {
      fprintf(f, "%d", world[i][j]);
      fputc('|', f);
    }
    fputc('\n', f);
  }
  fclose(f);
  printf("Fin de la sauvegarde");
}
