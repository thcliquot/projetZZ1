/**
 * @file save.h
 */

#ifndef SAVE_H
#define SAVE_H

#include "moteur.h"
#include <stdio.h>
#include <stdlib.h>

void load(char *file, moteur_t *mot);
void save(int **world, moteur_t *mot);

#endif /* SAVE_H */
