#include "UI.h"
#include "moteur.h"
#include "moteur_jeu_vie.h"
#include "save.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

/**
 *   \file main.c
 *   \brief code c principal
 *
 */

/**
 * \brief Fonction d'affichage des boutons du menu
 * 
 * \param graph 
 * \param text1 Le texte à écrire sur le 1er bouton
 * \param text2 Le texte à écrire sur le 2ème bouton
 */

void drawAcc(graphique_t *graph, SDL_Texture *text1, SDL_Texture *text2) {
  SDL_SetRenderDrawColor(graph->rend, 0, 0, 0, 255);
  SDL_RenderClear(graph->rend);

  SDL_Rect btn1;
  SDL_Rect btn2;

  btn1.x = graph->height / 8;
  btn1.y = 5 * (graph->height / 8);
  btn1.w = graph->width / 4;
  btn1.h = graph->height / 8;

  btn2.x = 5 * (graph->width / 8);
  btn2.y = 5 * (graph->height / 8);
  btn2.w = graph->width / 4;
  btn2.h = graph->height / 8;

  SDL_SetRenderDrawColor(graph->rend, 255, 255, 255, 255);
  SDL_RenderFillRect(graph->rend, &btn1);
  SDL_RenderFillRect(graph->rend, &btn2);

  btn1.x += 10;
  btn1.y += 10;
  btn1.w -= 20;
  btn1.h -= 20;

  btn2.x += 10;
  btn2.y += 10;
  btn2.w -= 20;
  btn2.h -= 20;

  SDL_RenderCopy(graph->rend, text1, NULL, &btn1);
  SDL_RenderCopy(graph->rend, text2, NULL, &btn2);

  SDL_RenderPresent(graph->rend);
}

/**
 * @brief Fonction d'affichage de la grille
 * 
 * @param graph
 * @param mot
 */

void drawJeu(graphique_t *graph, moteur_t *mot) {

  SDL_SetRenderDrawColor(graph->rend, 50, 50, 50, 255);
  SDL_RenderClear(graph->rend);
  SDL_SetRenderDrawColor(graph->rend, 0, 0, 0, 255);
  SDL_Rect rect;
  rect.x = 0;
  rect.y = 0;
  rect.w = mot->cellW * mot->largeur;
  rect.h = mot->cellH * mot->hauteur;
  SDL_RenderFillRect(graph->rend, &rect);

  SDL_SetRenderDrawColor(graph->rend, 255, 255, 255, 255);

  for (int i = 0; i < mot->hauteur; i++) {
    for (int j = 0; j < mot->largeur; j++) {
      if (mot->tabAct[i + mot->type][j + mot->type] == 1) {
        rect.x = j * mot->cellW;
        rect.y = i * mot->cellH;
        rect.w = mot->cellW;
        rect.h = mot->cellH;
        SDL_RenderFillRect(graph->rend, &rect);
      }
    }
  }
  SDL_RenderPresent(graph->rend);
}

/**
 * @brief Affichage de l'état statique
 * 
 * @param graph
 * @param mot
 * @param text1 le texte à afficher
 */

void drawSta(graphique_t *graph, moteur_t *mot, SDL_Texture *text1) {
  drawJeu(graph, mot);
  graph->rect.w = graph->width / 2;
  graph->rect.h = 100;
  graph->rect.x = graph->width / 4;
  graph->rect.y = graph->height / 2 - 50;

  SDL_RenderCopy(graph->rend, text1, NULL, &graph->rect);
  SDL_RenderPresent(graph->rend);
}

/**
 * @brief event global
 * 
 * @param graph 
 * @param mot 
 */

void eventGlobal(graphique_t *graph, moteur_t *mot) {
  switch (mot->event.type) {
  case SDL_QUIT:
    mot->running = SDL_FALSE;
    break;
  case SDL_WINDOWEVENT:
    switch (mot->event.window.event) {
    case SDL_WINDOWEVENT_CLOSE:
      mot->running = SDL_FALSE;
      break;
    case SDL_WINDOWEVENT_SIZE_CHANGED:
      graph->width = mot->event.window.data1;
      graph->height = mot->event.window.data2;
      mot->cellW = mot->cellH =
          graph->width / mot->largeur < graph->height / mot->hauteur
              ? graph->width / mot->largeur
              : graph->height / mot->hauteur;
      break;
    }
    break;
  case SDL_KEYDOWN:
    switch (mot->event.key.keysym.sym) {
    case SDLK_ESCAPE:
      mot->running = SDL_FALSE;
      break;
    }
  }
}


/**
 * @brief Fonction d'event pour les boutons
 * 
 * @param graph 
 * @param mot 
 * @param texte1 le texte du 1er bouton
 * @param texte2 le texte du 2ème bouton
 */

void eventFuncAcc(graphique_t *graph, moteur_t *mot, SDL_Texture *texte1,
                  SDL_Texture *texte2) {
  while (SDL_PollEvent(&mot->event)) {
    eventGlobal(graph, mot);

    switch (mot->event.type) {
    case SDL_MOUSEBUTTONDOWN:
      if (SDL_BUTTON(SDL_BUTTON_LEFT)) {
        int x, y;
        SDL_GetMouseState(&x, &y);
        if ((graph->height * 5 / 8 <= y && 6 * graph->height / 8 >= y)) {
          if (graph->width * 5 / 8 <= x && graph->width * 7 / 8 >= x) {
            mot->etat = CREATION;
            mot->type = 0;
          } else if (graph->width / 8 <= x && (graph->width * 3) / 8 >= x) {
            mot->etat = CREATION;
            mot->type = 1;
          }
        }
      }
      break;
    case SDL_WINDOWEVENT:
      switch (mot->event.window.event) {
      case SDL_WINDOWEVENT_SIZE_CHANGED:
        drawAcc(graph, texte1, texte2);
        break;
      }
      break;
    }
  }
}

/**
 * @brief Fonction d'event de l'état statique
 * 
 * @param graph 
 * @param mot 
 * @param texte1 Le texte à afficher
 */

void eventFuncSta(graphique_t *graph, moteur_t *mot, SDL_Texture *texte1) {
  while (SDL_PollEvent(&mot->event)) {
    eventGlobal(graph, mot);

    switch (mot->event.type) {
    case SDL_WINDOWEVENT:
      switch (mot->event.window.event) {
      case SDL_WINDOWEVENT_SIZE_CHANGED:
        drawSta(graph, mot, texte1);
        break;
      }
      break;
    }
  }
}

/**
 * @brief Fonction d'event lors de la séléction de l'état de départ de la matrice
 * 
 * @param graph 
 * @param mot 
 */

void eventFuncCre(graphique_t *graph, moteur_t *mot) {
  while (SDL_PollEvent(&mot->event)) {
    eventGlobal(graph, mot);

    switch (mot->event.type) {
    case SDL_MOUSEBUTTONDOWN:
      if (SDL_BUTTON(SDL_BUTTON_LEFT)) {
        int x, y;
        SDL_GetMouseState(&x, &y);
        x = x / mot->cellW > mot->largeur - 1 ? mot->largeur - 1
                                              : x / mot->cellW;
        y = y / mot->cellH > mot->hauteur - 1 ? mot->hauteur - 1
                                              : y / mot->cellH;
        mot->tabAct[y + mot->type][x + mot->type] =
            (mot->tabAct[y + mot->type][x + mot->type] + 1) % 2;
      }
      break;
    case SDL_KEYDOWN:
      switch (mot->event.key.keysym.sym) {
      case SDLK_RETURN:
        mot->etat = JEU;
        break;
      case SDLK_s:
        save(mot->tabAct, mot);
        break;
      }
    }
  }
}

/**
 * @brief Fonction d'event lorsque le jeu de la vie tourne
 * 
 * @param graph 
 * @param mot 
 */

void eventFuncJeu(graphique_t *graph, moteur_t *mot) {
  while (SDL_PollEvent(&mot->event)) {
    eventGlobal(graph, mot);

    switch (mot->event.type) {
    case SDL_KEYDOWN:
      switch (mot->event.key.keysym.sym) {
      case SDLK_LEFT:
      case SDLK_q: // Déplacement gauche
        mot->speed = (mot->speed + 10) >= 10000 ? 10000 : mot->speed + 10;
        break;
      case SDLK_RIGHT:
      case SDLK_d: // Déplacement droite
        mot->speed = (mot->speed - 10) <= 0 ? 0 : mot->speed - 10;
        break;
      case SDLK_SPACE: // Changement couleur
        mot->etat = PAUSE;
        break;
      case SDLK_s:
        save(mot->tabAct, mot);
        break;
      }
      break;
    }
  }
}

/**
 * @brief Fonction d'event quand le jeu est en pause
 * 
 * @param graph 
 * @param mot 
 */

void eventFuncPau(graphique_t *graph, moteur_t *mot) {
  while (SDL_PollEvent(&mot->event)) {
    eventGlobal(graph, mot);

    switch (mot->event.type) {
    case SDL_KEYDOWN:
      switch (mot->event.key.keysym.sym) {
      case SDLK_SPACE:
        mot->etat = JEU;
        break;
      case SDLK_s:
        save(mot->tabAct, mot);
        break;
      }
    }
    break;
  }
}

int main(int argc, char *argv[]) {

  graphique_t *graph = malloc(sizeof(graphique_t));
  moteur_t *mot = malloc(sizeof(moteur_t));

  SDL_Texture *text1;
  SDL_Texture *text2;
  TTF_Font *font;

  mot->etat = ACCUEIL;
  mot->running = SDL_TRUE;
  mot->speed = 100;
  mot->type = 0;
  mot->largeur = 1;
  mot->hauteur = 1;

  initialiserGraphique(graph, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                       900, 900);

  initialiserTexte(&text1, &font, graph->rend, "Night.ttf", "monde avec bord");
  initialiserTexte(&text2, &font, graph->rend, "Night.ttf", "monde torique");

  drawAcc(graph, text1, text2);

  while (mot->running && mot->etat == ACCUEIL) {      //etat démarrer, on attend un choix de bouton
    eventFuncAcc(graph, mot, text1, text2);
    SDL_Delay(10);
  }

  if (argc > 1) {                 //cas ou on load un fichier
    load(argv[1], mot);

    mot->cellW = mot->cellH =
        graph->width / mot->largeur < graph->height / mot->hauteur
            ? graph->width / mot->largeur
            : graph->height / mot->hauteur;

    mot->etat = JEU;

  } else {                      //si pas de load on demande les paramètres

    printf("Veuillez entrez le nombre de ligne maximal.\n");
    if (!scanf("%d%*c", &mot->hauteur)) {
      fprintf(stderr, "FAIL TO READ THE INTEGER\n");
      exit(-1);
    }
    printf("Veuillez entrez le nombre de colonne maximal.\n");
    if (!scanf("%d%*c", &mot->largeur)) {
      fprintf(stderr, "FAIL TO READ THE INTEGER\n");
      exit(-1);
    }

    mot->cellW = mot->cellH =
        graph->width / mot->largeur < graph->height / mot->hauteur
            ? graph->width / mot->largeur
            : graph->height / mot->hauteur;

    mot->tabAct = myalloc2D(
        mot->hauteur + 2 * mot->type,
        mot->largeur +
            2 * mot->type); // next_tab, tableau pour effectuer le changement
    mot->tabPrec = myalloc2D(
        mot->hauteur + 2 * mot->type,
        mot->largeur + 2 * mot->type); // tableau contenant les valeurs du jeu

    printf("Veuillez cliquer sur les cases pour les changer d'état, une fois"
           " finis appuyez sur entrée\n");

    while (mot->running && mot->etat == CREATION) {     //etat de choix des case à allumer
      eventFuncCre(graph, mot);
      drawJeu(graph, mot);
      SDL_Delay(10);
    }
  }

  while (mot->running && mot->etat != STABLE) {
    switch (mot->etat) {
    case JEU:                                           //si on est en jeu alors on itére
      eventFuncJeu(graph, mot);
      drawJeu(graph, mot);
      if (next(&mot->tabAct, &mot->tabPrec, mot->type, mot->hauteur,
               mot->largeur)) {
        mot->etat = STABLE;
      }
      break;
    case PAUSE:                                        //si on est en pause
      eventFuncPau(graph, mot);                       
      break;
    default:
      break;
    }
    SDL_Delay(mot->speed);
  }

  initialiserTexte(&text1, &font, graph->rend, "Night.ttf", "Etat stable");

  if (mot->etat == STABLE) {                          //etat stable, on arrête de faire des calculs inutiles
    printf("Etat Stable\n");
    drawSta(graph, mot, text1);
  }

  while (mot->running) {
    eventFuncSta(graph, mot, text1);
  }

  myfree2D(mot->tabAct, mot->hauteur + 2 * mot->type);
  myfree2D(mot->tabPrec, mot->hauteur + 2 * mot->type);
  freeGraphique(graph);
  SDL_DestroyTexture(text1);
  SDL_DestroyTexture(text2);
  TTF_Quit();
  SDL_Quit();

  return 0;
}
