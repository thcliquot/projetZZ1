/**
 * @file moteur_jeu_vie.h
 */

#ifndef moteur_jeu_vie_h
#define moteur_jeu_vie_h

#include "moteur.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int **myalloc2D(int, int);
void myfree2D(int **, int);
void afficher_tab(int **, int, int, int);
int neighbors(int **, int, int, int, int);
int next(int ***, int ***, int, int, int);

#endif
