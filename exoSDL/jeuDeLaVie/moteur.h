/**
 * @file moteur.h
 * \brief Fichier contenant la structure du moteur
 */

#ifndef MOTEUR_H
#define MOTEUR_H
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <stdlib.h>

/**
 * \enum ETAT
 * @brief Etat du jeu
 * 
 * Les différents états dans lequel se trouve le jeu.
 * En pause, à l'acceuil ...
 * 
 */

typedef enum ETAT {
  ACCUEIL = 0,
  CREATION = 1,
  JEU = 2,
  PAUSE = 3,
  STABLE = 4
} ETAT;

/**
 *
 * \struct moteur_t
 * @brief Paramétre du moteur
 * 
 * Contient la taille des fenêtres manipuler par
 * le moteur du jeu de la vie et autres
 * 
 */

typedef struct {
  SDL_Event event;
  ETAT etat;
  int speed;
  SDL_bool running;
  int type;
  int **tabAct;
  int **tabPrec;
  int hauteur;
  int largeur;
  int cellW;
  int cellH;

} moteur_t;

#endif /* MOTEUR_H */
