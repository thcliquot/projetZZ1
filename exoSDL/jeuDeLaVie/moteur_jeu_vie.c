/**
 * @file moteur_jeu_vie.c
 * @author Oscar M, Antoine A, Théo C
 * @brief Moteur du jeu de la vie
 * @date 2022-06-22
 * 
 */

#include "moteur_jeu_vie.h"

/**
 * \param survive les régles de survie
 */
int survive[9] = {0, 0, 0, 1, 1, 1, 1, 0, 0};

/**
 * \param birth les régles de naissance
*/

int birth[9] = {0, 0, 0, 1, 1, 1, 0, 0, 0};

/**
 * \param to_check le nombre de voisins à regarder
*/

int to_check_nbr = 8;

/**
 * \param to_check_nbr les coordonnées du voisin relative à la case regardée
*/

int to_check[8][2] = {{-1, -1}, {-1, 0}, {-1, 1}, {1, -1}, {1, 0}, {1, 1}, {0, -1}, {0, 1}};


/**
 * @brief Fonction d'allocation d'une matrice (vide)
 * 
 * @param hauteur 
 * @param largeur 
 * @return int** le tableau créé
 */

int **myalloc2D(int hauteur, int largeur) {
  int **tab = malloc(sizeof(int *) * hauteur);
  for (int i = 0; i < hauteur; i++) {
    tab[i] = calloc(largeur, sizeof(int *));
  }
  return tab;
}


/**
 * @brief Fonction de libération d'une matrice
 * 
 * @param tab 
 * @param hauteur 
 */

void myfree2D(int **tab, int hauteur) {
  for (int i = 0; i < hauteur; i++) {
    free(tab[i]);
  }
  free(tab);
}

/**
 * @brief Fonction d'affichage d'une matrice
 * 
 * @param tab 
 * @param type 
 * @param hauteur 
 * @param largeur 
 */

void afficher_tab(int **tab, int type, int hauteur, int largeur) {
  int i, j;
  for (i = type; i < hauteur + type; i++) {
    printf("\n");
    for (j = type; j < largeur + type; j++) {
      printf("%d ", tab[i][j]);
    }
  }
  printf("\n===================");
}

/**
 * @brief Fonction qui compte le nombre de voisins
 * 
 * @param tab 
 * @param i coordonnée en x de la case centrale
 * @param j coordonnée en y de la case centrale
 * @param hauteur 
 * @param largeur 
 * @return int le nombre de voisins de la case
 */

int neighbors(int **tab, int i, int j, int hauteur, int largeur) {
  int k;
  int sum = 0;
  for (k = 0; k < to_check_nbr; k++) {
    sum = sum + tab[(i + to_check[k][0] + hauteur) % hauteur]
                   [(j + to_check[k][1] + largeur) % largeur];
  }
  return sum;
}

/**
 * @brief Fonction qui donne l'itération suivante du jeu de la vie
 * 
 * @param tab tableau qui contient l'itération k
 * @param ntab tableau qui sert à avoir k+1, avant qu'on échange tab et ntab
 * @param type 
 * @param hauteur 
 * @param largeur 
 * @return int 
 */

int next(int ***tab, int ***ntab, int type, int hauteur, int largeur) {
  int i, j, val;
  int **tmp;
  int stable = 1;

  for (i = type; i < hauteur + type; i++) {
    for (j = type; j < largeur + type; j++) {
      val = neighbors(*tab, i, j, hauteur + 2 * type, largeur + 2 * type);
      if ((*tab)[i][j]) { // en fonction de la valeur de la case regardé
        (*ntab)[i][j] = survive[val]; // on regarde si elle survit
      } else {
        (*ntab)[i][j] = birth[val]; // ou si elle meurt
      }
      if ((*ntab)[i][j] != (*tab)[i][j])
        stable = 0;
    }
  }
  tmp = *tab;
  *tab = *ntab;
  *ntab = tmp;

  return stable;
}
