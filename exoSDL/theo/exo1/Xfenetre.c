#include "UI.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {

  /*============================================================
   *|                     Initialisation                       |
   *============================================================*/

  int *posActualX = malloc(4); // abscisse de la fenêtre en cours
  int *posActualY = malloc(4); // ordonnée de la fenêtre en cours
  int w, h;                    // taille de la fenêtre (width and height)
  int direction = 5;           // direction des fenêtres
  int winHeight;               // taille d'une fenêtre
  int winWidth;                // ...

  SDL_Window *arrWin[10]; // tableau contenant toutes les fenêtres
  SDL_Renderer
      *arrRend[10]; // Création d'un renderer à cause d'un problème de fluidité
  SDL_Event event;             // gestion des events
  SDL_bool running = SDL_TRUE; // indique si notre programme doit tourner

  initialiserSDL();

  SDL_DisplayMode DM;
  if (SDL_GetDesktopDisplayMode(0, &DM) != 0) {
    SDL_Log("SDL_GetDesktopDisplayMode failed: %s", SDL_GetError());
    return 1;
  }

  w = DM.w;
  h = DM.h;
  winHeight = h / 11;
  winWidth = w / 10;

  printf("Taille de l'écran : %dX%d\n", w, h);

  // Boucle pour initialiser nos fenêtres (et renderer)
  for (int i = 0; i < 10; i++) {
    initialiserWindows(&arrWin[i], 100, winHeight * i + 20, winWidth,
                       winHeight);
    initialiserRenderer(&arrRend[i], arrWin[i]);
  }

  /*============================================================
   *|                     Code Principal                       |
   *============================================================*/

  // Boucle principal du programme
  while (running) {

    // Gestion des événements
    if (SDL_PollEvent(&event)) {
      switch (event.type) {
      case SDL_QUIT: // Si on quitte la fenêtre, fin de la boucle
        running = SDL_FALSE;
        break;
      case SDL_KEYDOWN:
        switch (event.key.keysym.sym) {
        case SDLK_ESCAPE: // De même avec la touche echap
          running = SDL_FALSE;
          break;
        }
      default:
        break;
      }

      // Boucle pour bouger nos fenêtres
      for (int i = 0; i < 10; i++) {
        SDL_GetWindowPosition(arrWin[i], posActualX, posActualY);
        SDL_SetWindowPosition(arrWin[i], *posActualX + direction, *posActualY);
      }

      // Changement de direction si on atteins un des bords
      if (*posActualX + 2 * direction < 100 ||
          (*posActualX + 2 * direction > w - 100)) {
        direction *= -1;
      }
    }
  }

  /*============================================================
   *|                       Libération                         |
   *============================================================*/

  for (int i = 0; i < 10; i++) {
    SDL_DestroyWindow(arrWin[i]);
  }

  free(posActualX);
  free(posActualY);

  SDL_Quit();
  return 0;
}
