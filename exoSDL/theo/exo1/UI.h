/**
 *   \file UI.h
 *   \brief code h regroupant certaines fonctions pour manier le côté graphique
 *   de la sdl
 *
 *  Ne concerne pas tous ce qui va toucher au moteur (event ...) et reste assez
 *  globale pour une utilisation dans la plupart des programmes.
 *
 */

#ifndef __UI__
#define __UI__

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <stdlib.h>

/**
 *
 * \struct graphique_t
 * \brief structure permettant un résultat graphique simple
 *
 * Peut ne pas suffire
 *
 **/

typedef struct {
  SDL_Window *window;
  SDL_Renderer *rend;
  int size;
  SDL_Rect rect;
  SDL_Texture *fondIntro;
  SDL_Texture *text;
  TTF_Font *font;
} graphique_t;

/**
 *
 * \fn void initialiserSDL()
 * \brief initialise la SDL (indispensable)
 * \param None
 * \return Null
 *
 **/

void initialiserSDL();

/**
 *
 * \fn void initialiserWindows(SDL_Window **w, int posX, int posY, int x, int y)
 * \brief initialise une fenêtre avec tous les arguments
 * \param SDL_Window ** w: adresse du pointeur de fenêtre à initialiser
 * \param int posX : position de la fenêtre sur l'axe horizontal
 * \param int posY: position de la fenêtre sur l'axe vertical
 * \param int x: taille de la fenêtre (horizontale)
 * \param int y: taille de la fenêtre (verticale)
 * \return Null
 *
 * A modifier si on veut plus de flag
 *
 **/

void initialiserWindows(SDL_Window **w, int posX, int posY, int x, int y);

/**
 *
 * \fn initialiserRenderer(SDL_Renderer **r,SDL_Window *w)
 * \brief initialise un renderer
 * \param SDL_Renderer ** r: adresse du pointeur renderer à initialiser
 * \param SDL_Window *w : sur quel fenêtre est notre renderer
 * \return Null
 *
 * Sortie erreur plus propre possible (free)
 *
 **/

void initialiserRenderer(SDL_Renderer **r, SDL_Window *w);

/**
 *
 * \fn initialiserImage(SDL_Texture **t, char *path, SDL_Renderer *r)
 * \brief initialise une image avec comme chemin path
 * \param SDL_Texture **t : adresse du pointeur de texture (ici image) à
 initialiser
 * \param char* path : Le chemin de l'image
 * \param SDL_Renderer * r: sur quel renderer est notre image;
 *
 * \return Null
 *
 * Sortie erreur plus propre possible (free)
 *
 **/

void initialiserImage(SDL_Texture **t, char *path, SDL_Renderer *r);

/**
 *
 * \fn initialiserTexte(SDL_Texture **t, TTF_Font *font, SDL_Renderer *r)
 * \brief initialise un texte avec un font choisi
 * \param SDL_Texture **t : adresse du pointeur de texture (ici texte) à
 initialiser
 * \param TTF_FONT *font : Le chemin vers le font (*.ttf)
 * \param SDL_Renderer * r: sur quel renderer est notre texte;
 *
 * \return Null
 *
 * Sortie erreur plus propre possible (free)
 *
 **/

void initialiserTexte(SDL_Texture **t, TTF_Font *font, SDL_Renderer *r);

/**
 *
 * \fn initialiserGraphique(graphique_t *g)
 * \brief initialise une structure graphique_t
 * \param graphique_t *g: la structure à initialiser
 * \return Null
 *
 **/

void initialiserGraphique(graphique_t *g);

/**
 *
 * \fn freeGraphique(graphique_t *g)
 * \brief libère une structure graphique_t
 * \param graphique_t *g: la structure à libèrer
 * \return Null
 *
 **/

void freeGraphique(graphique_t *g);

#endif
