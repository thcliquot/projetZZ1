#include "UI.h"

int main() {

  int dimW, dimH;
  int dimWinW = 900, dimWinH = 900;
  SDL_Event event;
  int i = 0, j = 0;

  graphique_t *g = malloc(sizeof(graphique_t));

  initialiserGraphique(g, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 900,
                       900);

  SDL_Rect rectFond;
  SDL_Rect rectSprite;

  rectFond.x = 0;
  rectFond.y = 0;
  rectFond.w = dimWinW;
  rectFond.h = dimWinH;

  SDL_Texture *fond = NULL;
  SDL_Texture *sprite = NULL;
  SDL_bool running = SDL_TRUE;

  initialiserImage(&fond, "img/terrain.png", g->rend);
  initialiserImage(&sprite, "img/defender_siegius.png", g->rend);

  SDL_QueryTexture(sprite, NULL, NULL, &dimW, &dimH);

  int widthS = 210, heightS = 210;

  rectSprite.x = (dimWinW - widthS) / 2;
  rectSprite.y = (dimWinH - heightS) / 2;
  rectSprite.w = widthS;
  rectSprite.h = heightS;

  g->rect.x = (i * (widthS + 40) + 20);
  g->rect.y = (j * (heightS + 55) + 50);
  g->rect.w = widthS;
  g->rect.h = heightS;

  while (running) {
    while (SDL_PollEvent(&event)) {
      switch (event.type) {
      case SDL_QUIT:
        running = SDL_FALSE;
        break;

      default:
        break;
      }
    }

    SDL_RenderCopy(g->rend, fond, NULL, &rectFond);
    SDL_RenderCopy(g->rend, sprite, &g->rect, &rectSprite);
    SDL_RenderPresent(g->rend);
    i++;
    if (i > 7) {
      j++;
      i = 0;
    }
    if (j > 1) {
      j = 0;
    }
    g->rect.x = (i * (widthS + 40) + 20);
    g->rect.y = (j * (heightS + 55) + 50);
    g->rect.w = widthS;
    g->rect.h = heightS;
    SDL_Delay(250);
  }
}
