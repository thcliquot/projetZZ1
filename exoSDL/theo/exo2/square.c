#include "UI.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define N 10
int main() {

  /*============================================================
   *|                     Initialisation                       |
   *============================================================*/

  srand(time(NULL));

  SDL_Window *window;          // La fenêtre principal
  SDL_Renderer *rend;          // Le seul renderer utilisé
  SDL_Rect rect;               // Un rectangle (seul forme)
  SDL_bool running = SDL_TRUE; // Indique si le programme tourne
  SDL_Event event;             // pour gérer les événements

  int w, h; // taille de la fenêtre
  int r = rand() % 255, g = rand() % 255, b = rand() % 255; //(red green blue)

  int dr = 1, dg = 2, db = 3; // (indicateur de modification de r g et b)

  int posX = 0, posY = 0; // position de notre carré actuelle
  int directionX = 1,
      directionY = 0; // direction que l'utilisateur veut emprunter

  int color = 0; // choix de la couleur

  initialiserSDL();
  initialiserWindows(&window, 0, 0, 600, 600);
  initialiserRenderer(&rend, window);

  SDL_GetWindowSize(window, &w, &h);

  printf("Window size: %dX%d\n", w, h);

  rect.w = w / N; // taille des rectangles
  rect.h = h / N;

  SDL_SetRenderDrawColor(rend, 0, 0, 0, 255);
  SDL_RenderClear(rend);
  SDL_RenderPresent(rend);

  /*============================================================
   *|                     Code Principal                       |
   *============================================================*/

  // Boucle principale
  while (running) {
    if (SDL_PollEvent(&event)) {
      switch (event.type) {
      case SDL_QUIT:
        running = SDL_FALSE;
        break;
      case SDL_KEYDOWN:
        switch (event.key.keysym.sym) {
        case SDLK_ESCAPE:
          running = SDL_FALSE;
          break;
        case SDLK_LEFT:
        case SDLK_q: // Déplacement gauche
          directionX = -1;
          directionY = 0;
          break;
        case SDLK_RIGHT:
        case SDLK_d: // Déplacement droite
          directionX = 1;
          directionY = 0;
          break;
        case SDLK_UP:
        case SDLK_z: // Déplacement haut
          directionY = -1;
          directionX = 0;
          break;
        case SDLK_DOWN:
        case SDLK_s: // Déplacement bas
          directionY = 1;
          directionX = 0;
          break;
        case SDLK_SPACE: // Changement couleur
          color = (color + 1) % 3;
          break;
        }
        break;
      }
    }

    // Nouvelle position abscisse
    posX = posX + directionX < 0
               ? 0
               : posX + directionX >= N ? N - 1 : posX + directionX;

    // Nouvelle position ordonnée
    posY = posY + directionY < 0
               ? 0
               : posY + directionY >= N ? N - 1 : posY + directionY;
    rect.x = posX * rect.w;
    rect.y = posY * rect.h;

    // Modification de la couleur en cours
    r = color == 0 ? (int)(r + dr) % 255 : 0;
    g = color == 1 ? (int)(g + dg) % 255 : 0;
    b = color == 2 ? (int)(b + db) % 255 : 0;

    // Afficher le bloc de couleur
    SDL_SetRenderDrawColor(rend, r, g, b, 255);
    SDL_RenderFillRect(rend, &rect);
    SDL_RenderPresent(rend);

    // Remise à 0 tant qu'il n'y as pas un autre appui
    directionX = 0;
    directionY = 0;

    SDL_Delay(10);
  }

  /*============================================================
   *|                       Libération                         |
   *============================================================*/

  SDL_DestroyRenderer(rend);
  SDL_DestroyWindow(window);
  SDL_Quit();

  return 0;
}
