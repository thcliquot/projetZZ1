#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

void end_sdl(char ok,				 // fin normale : ok = 1 ; anormale ok = 0
			 char const *msg,		 // message à afficher
			 SDL_Window *window,	 // fenêtre à fermer
			 SDL_Renderer *renderer) // renderer à fermer
{
	char msg_formated[255];
	int l;

	if (!ok)
	{ // Affichage de ce qui ne va pas
		strncpy(msg_formated, msg, 250);
		l = strlen(msg_formated);
		strcpy(msg_formated + l, " : %s\n");

		SDL_Log(msg_formated, SDL_GetError());
	}

	if (renderer != NULL)
	{								   // Destruction si nécessaire du renderer
		SDL_DestroyRenderer(renderer); // Attention : on suppose que les NULL sont maintenus !!
		renderer = NULL;
	}
	if (window != NULL)
	{							   // Destruction si nécessaire de la fenêtre
		SDL_DestroyWindow(window); // Attention : on suppose que les NULL sont maintenus !!
		window = NULL;
	}

	IMG_Quit();
	SDL_Quit();

	if (!ok)
	{ // On quitte si cela ne va pas
		exit(EXIT_FAILURE);
	}
}

SDL_Texture *load_texture_from_image(char *file_image_name, SDL_Window *window, SDL_Renderer *renderer)
{
	SDL_Surface *my_image = NULL;	// Variable de passage
	SDL_Texture *my_texture = NULL; // La texture

	my_image = IMG_Load(file_image_name); // Chargement de l'image dans la surface
										  // image=SDL_LoadBMP(file_image_name); fonction standard de la SDL,
										  // uniquement possible si l'image est au format bmp */
	if (my_image == NULL)
		end_sdl(0, "Chargement de l'image impossible", window, renderer);

	my_texture = SDL_CreateTextureFromSurface(renderer, my_image); // Chargement de l'image de la surface vers la texture
	SDL_FreeSurface(my_image);									   // la SDL_Surface ne sert que comme élément transitoire
	if (my_texture == NULL)
		end_sdl(0, "Echec de la transformation de la surface en texture", window, renderer);

	return my_texture;
}

void play_with_texture_1_1(SDL_Texture *my_texture, SDL_Window *window,
						   SDL_Renderer *renderer)
{
	SDL_Rect
		source = {0},			 // Rectangle définissant la zone de la texture à récupérer
		window_dimensions = {0}, // Rectangle définissant la fenêtre, on n'utilisera que largeur et hauteur
		destination = {0};		 // Rectangle définissant où la zone_source doit être déposée dans le renderer

	SDL_GetWindowSize(
		window, &window_dimensions.w,
		&window_dimensions.h); // Récupération des dimensions de la fenêtre
	SDL_QueryTexture(my_texture, NULL, NULL,
					 &source.w, &source.h); // Récupération des dimensions de l'image

	destination = window_dimensions; // On fixe les dimensions de l'affichage à  celles de la fenêtre

	/* On veut afficher la texture de façon à ce que l'image occupe la totalité de la fenêtre */

	SDL_RenderCopy(renderer, my_texture,
				   &source,
				   &destination); // Création de l'élément à afficher
}

void play_with_texture_4_2(SDL_Texture *bg_texture,
						   SDL_Texture *my_texture,
						   SDL_Window *window,
						   SDL_Renderer *renderer)
{
	SDL_Rect
		source = {0},			 // Rectangle définissant la zone totale de la planche
		window_dimensions = {0}, // Rectangle définissant la fenêtre, on n'utilisera que largeur et hauteur
		destination = {0},		 // Rectangle définissant où la zone_source doit être déposée dans le renderer
		state = {0};			 // Rectangle de la vignette en cours dans la planche

	SDL_GetWindowSize(window, // Récupération des dimensions de la fenêtre
					  &window_dimensions.w,
					  &window_dimensions.h);
	SDL_QueryTexture(my_texture, // Récupération des dimensions de l'image
					 NULL, NULL,
					 &source.w, &source.h);

	/* Mais pourquoi prendre la totalité de l'image, on peut n'en afficher qu'un morceau, et changer de morceau :-) */

	int nb_images = 10;					 // Il y a 8 vignette dans la ligne de l'image qui nous intéresse
	float zoom = 1;						 // zoom, car ces images sont un peu petites
	int offset_x = source.w / nb_images, // La largeur d'une vignette de l'image, marche car la planche est bien réglée
		offset_y = source.h / 1;		 // La hauteur d'une vignette de l'image, marche car la planche est bien réglée

	state.x = 0;			// La première vignette est en début de ligne
	state.y = 0 * offset_y; // une seule ligne
	state.w = offset_x;		// Largeur de la vignette
	state.h = offset_y;		// Hauteur de la vignette

	destination.w = offset_x * zoom; // Largeur du sprite à l'écran
	destination.h = offset_y * zoom; // Hauteur du sprite à l'écran

	destination.y = // animation au milieu de l'écran
		(window_dimensions.h - destination.h) / 2;

	for (int i = 0; i < nb_images; i++)
	{
		SDL_RenderClear(renderer); 				// Effacer l'image précédente avant de dessiner la nouvelle
		play_with_texture_1_1(bg_texture, window, renderer);
		SDL_RenderCopy(renderer, my_texture, 	// Préparation de l'affichage
					   &state,
					   &destination);
		SDL_RenderPresent(renderer); 			// Affichage
		state.x += offset_x; 					// On passe à la vignette suivante dans l'image
		SDL_Delay(200);				 		// Pause en ms
	}
	SDL_RenderClear(renderer); // Effacer la fenêtre avant de rendre la main
}

int main()
{

	if (SDL_Init(SDL_INIT_VIDEO) != 0)
	{
		SDL_Log("Error : SDL initialisation - %s\n",
				SDL_GetError()); // l'initialisation de la SDL a échoué
		SDL_Quit();
		exit(EXIT_FAILURE);
	}

	SDL_Window *window = NULL;
	SDL_Renderer *renderer = NULL;

	window = SDL_CreateWindow("première animation", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 600, 400, 0);
	if (window == NULL)
		end_sdl(0, "ERROR WINDOW CREATION", window, renderer);

	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (renderer == NULL)
		end_sdl(0, "ERROR RENDERER CREATION", window, renderer);

	SDL_Texture *texture = load_texture_from_image("animation_transparence.png", window, renderer);
	SDL_Texture *fond = load_texture_from_image("fond.bmp", window, renderer);

	play_with_texture_4_2(fond, texture, window, renderer);

	SDL_DestroyTexture(texture);
	SDL_DestroyTexture(fond);

	end_sdl(1, "Normal ending", window, renderer);

	return 0;
}