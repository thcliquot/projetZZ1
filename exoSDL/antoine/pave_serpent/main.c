#include <SDL2/SDL.h>
#include <math.h>
#include <stdio.h>
#include <string.h>

#define pas 5

/*********************************************************************************************************************/
/*                              Programme d'exemple de création de rendu + dessin                                    */
/*********************************************************************************************************************/
void end_sdl(char ok,			 		// fin normale : ok = 0 ; anormale ok = 1
			 char const *msg,	 		// message à afficher
			 SDL_Window *window, 		// fenêtre à fermer
			 SDL_Renderer *renderer)	// renderer à fermer
{
	char msg_formated[255];
	int l;

	if (!ok)
	{ // Affichage de ce qui ne va pas
		strncpy(msg_formated, msg, 250);
		l = strlen(msg_formated);
		strcpy(msg_formated + l, " : %s\n");

		SDL_Log(msg_formated, SDL_GetError());
	}

	if (renderer != NULL)
	{								   	// Destruction si nécessaire du renderer
		SDL_DestroyRenderer(renderer); 	// Attention : on suppose que les NULL sont maintenus !!
		renderer = NULL;
	}
	if (window != NULL)
	{							   		// Destruction si nécessaire de la fenêtre
		SDL_DestroyWindow(window); 		// Attention : on suppose que les NULL sont maintenus !!
		window = NULL;
	}

	SDL_Quit();

	if (!ok)
	{ // On quitte si cela ne va pas
		exit(EXIT_FAILURE);
	}
}

void draw(SDL_Renderer *renderer, int size, int win_w, int win_h)
{
	SDL_Rect rectangle;

	SDL_SetRenderDrawColor(renderer,
						   255, 255, 255,	// mode Red, Green, Blue (tous dans 0..255)
						   255);			// 0 = transparent ; 255 = opaque
	rectangle.x = (win_w / 2) - (size / 2); // x haut gauche du rectangle
	rectangle.y = (win_h / 2) - (size / 2); // y haut gauche du rectangle
	rectangle.w = size;						// sa largeur (w = width)
	rectangle.h = size;						// sa hauteur (h = height)

	SDL_RenderFillRect(renderer, &rectangle);
}

int main(int argc, char **argv)
{
	(void)argc;
	(void)argv;

	SDL_Window *window = NULL;
	SDL_Renderer *renderer = NULL;

	SDL_DisplayMode screen;

	/*********************************************************************************************************************/
	/*                         Initialisation de la SDL  + gestion de l'échec possible                                   */
	if (SDL_Init(SDL_INIT_VIDEO) != 0)
		end_sdl(0, "ERROR SDL INIT", window, renderer);

	SDL_GetCurrentDisplayMode(0, &screen);
	printf("Résolution écran\n\tw : %d\n\th : %d\n",
		   screen.w, screen.h);

	int win_w = 0.66 * screen.w;
	int win_h = 0.66 * screen.h;
	/* Création de la fenêtre */
	window = SDL_CreateWindow("Premier dessin",
							  SDL_WINDOWPOS_CENTERED,
							  SDL_WINDOWPOS_CENTERED, win_w,
							  win_h,
							  SDL_WINDOW_OPENGL);
	if (window == NULL)
		end_sdl(0, "ERROR WINDOW CREATION", window, renderer);

	/* Création du renderer */
	renderer = SDL_CreateRenderer(window, -1,
								  SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (renderer == NULL)
		end_sdl(0, "ERROR RENDERER CREATION", window, renderer);

	/*********************************************************************************************************************/
	/*                                     On dessine dans le renderer                                                   */
	/*********************************************************************************************************************/
	/*             Cette partie pourrait avantageusement être remplacée par la boucle évènementielle                     */
	SDL_bool program_on = SDL_TRUE;
	SDL_Event event;
	int size = win_h / 2;
	int etat = 0;
	while (program_on)
	{
		if (SDL_PollEvent(&event))
		{
			switch (event.type)
			{
			case SDL_QUIT:
				program_on = SDL_FALSE;
				break;

			case SDL_KEYDOWN:
				switch (event.key.keysym.sym)
				{
				case SDLK_ESCAPE:
				case SDLK_q:
					program_on = SDL_FALSE;
					break;

				default:
					break;
				}

			default:
				break;
			}
		}
		SDL_SetRenderDrawColor(renderer, 0, 0, 0, 100);
		SDL_RenderClear(renderer);
		draw(renderer, size, win_w, win_h);
		SDL_RenderPresent(renderer); // affichage
		if (etat == 0)
		{
			if (size > 10)
				size -= pas;
			else
				etat = 1;
		}
		else
		{
			if (size < win_h / 2)
				size += pas;
			else
				etat = 0;
		}
		SDL_Delay(30);
	}

	/* on referme proprement la SDL */
	end_sdl(1, "Normal ending", window, renderer);
	return EXIT_SUCCESS;
}