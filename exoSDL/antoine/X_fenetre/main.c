#include <SDL2/SDL.h>
#include <stdio.h>

#define dimFenetre 50
#define pas 5

int main(int argc, char **argv)
{
	(void)argc;
	(void)argv;

	/* Initialisation de la SDL  + gestion de l'échec possible */
	if (SDL_Init(SDL_INIT_VIDEO) != 0)
	{
		SDL_Log("Error : SDL initialisation - %s\n",
				SDL_GetError()); // l'initialisation de la SDL a échoué
		exit(EXIT_FAILURE);
	}

	/* Création des fenetres */
	SDL_Window *tab_fenetres[36];

	int i;

	//haut du carré
	for(i = 0 ; i < 10 ; i++)
	{
		tab_fenetres[i] = SDL_CreateWindow("flood", i*dimFenetre, 0, dimFenetre, dimFenetre, 0);
		if(tab_fenetres[i] == NULL)
		{
			SDL_Log("Error : SDL window %d creation - %s\n", i, SDL_GetError());
			for(int j = 0 ; j < i ; j++)
			{
				SDL_DestroyWindow(tab_fenetres[j]);
			}
			SDL_Quit();
			exit(EXIT_FAILURE);
		}
	}

	//bas du carré
	for(i = 10 ; i < 20 ; i++)
	{
		tab_fenetres[i] = SDL_CreateWindow("flood", (i-10)*dimFenetre, 9*dimFenetre, dimFenetre, dimFenetre, 0);
		if(tab_fenetres[i] == NULL)
		{
			SDL_Log("Error : SDL window %d creation - %s\n", i, SDL_GetError());
			for(int j = 0 ; j < i ; j++)
			{
				SDL_DestroyWindow(tab_fenetres[j]);
			}
			SDL_Quit();
			exit(EXIT_FAILURE);
		}
	}

	//gauche du carré
	for(i = 20 ; i < 28 ; i++)
	{
		tab_fenetres[i] = SDL_CreateWindow("flood", 0, (i-19)*dimFenetre,  dimFenetre, dimFenetre, 0);
		if(tab_fenetres[i] == NULL)
		{
			SDL_Log("Error : SDL window %d creation - %s\n", i, SDL_GetError());
			for(int j = 0 ; j < i ; j++)
			{
				SDL_DestroyWindow(tab_fenetres[j]);
			}
			SDL_Quit();
			exit(EXIT_FAILURE);
		}
	}

	//droite du carré
	for(i = 28 ; i < 36 ; i++)
	{
		tab_fenetres[i] = SDL_CreateWindow("flood", 9*dimFenetre, (i-27)*dimFenetre, dimFenetre, dimFenetre, 0);
		if(tab_fenetres[i] == NULL)
		{
			SDL_Log("Error : SDL window %d creation - %s\n", i, SDL_GetError());
			for(int j = 0 ; j < i ; j++)
			{
				SDL_DestroyWindow(tab_fenetres[j]);
			}
			SDL_Quit();
			exit(EXIT_FAILURE);
		}
	}

	/* boucle */
	SDL_DisplayMode mode;
	if(SDL_GetCurrentDisplayMode(0, &mode) == 0)
	{
		SDL_bool program_on = SDL_TRUE;
		SDL_Event event;
		int x,y;
		int etat = 0;
		while(program_on)
		{
			if(SDL_PollEvent(&event))
			{
				switch(event.type)
				{
					case SDL_QUIT:
					program_on = SDL_FALSE;
					break;

					case SDL_KEYDOWN:
						switch(event.key.keysym.sym)
						{
							case SDLK_ESCAPE:
							case SDLK_q:
								program_on = SDL_FALSE;
								break;
							
							default:
								break;
						}
					
					default:
						break;
				}
			}

			switch(etat)
			{
				case 0:
					SDL_GetWindowPosition(tab_fenetres[19], &x, &y);
					if(x+pas < mode.w)
					{
						for(i = 0 ; i < 36 ; i++)
						{
							SDL_GetWindowPosition(tab_fenetres[i], &x, &y);
							SDL_SetWindowPosition(tab_fenetres[i], x+pas, y);
						}
					}
					else etat = 1;
					break;

				case 1:
					SDL_GetWindowPosition(tab_fenetres[19], &x, &y);
					if(y+pas < mode.h)
					{
						for(i = 0 ; i < 36 ; i++)
						{
							SDL_GetWindowPosition(tab_fenetres[i], &x, &y);
							SDL_SetWindowPosition(tab_fenetres[i], x, y+pas);
						}
					}
					else etat = 2;
					break;

				case 2:
					SDL_GetWindowPosition(tab_fenetres[0], &x, &y);
					if(x-pas > 0)
					{
						for(i = 0 ; i < 36 ; i++)
						{
							SDL_GetWindowPosition(tab_fenetres[i], &x, &y);
							SDL_SetWindowPosition(tab_fenetres[i], x-pas, y);
						}
					}
					else etat = 3;
					break;
				
				case 3:
					SDL_GetWindowPosition(tab_fenetres[0], &x, &y);
					if(y-pas > 0)
					{
						for(i = 0 ; i < 36 ; i++)
						{
							SDL_GetWindowPosition(tab_fenetres[i], &x, &y);
							SDL_SetWindowPosition(tab_fenetres[i], x, y-pas);
						}
					}
					else etat = 0;
					break;
			}

			SDL_Delay(20);
		}
	}
	else
	{
		SDL_Log("Error : SDL get window position - %s\n", SDL_GetError());
	}


	for(i = 0 ; i < 36 ; i++)
	{
		SDL_DestroyWindow(tab_fenetres[i]);
	}
	SDL_Quit(); // la SDL

	return 0;
}